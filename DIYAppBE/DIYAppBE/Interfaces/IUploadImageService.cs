﻿namespace DIYAppBE.Interfaces
{
    public interface IUploadImageService
    {
        Task<string> UploadAsync(Stream fileStream, string fileName);
    }
}
