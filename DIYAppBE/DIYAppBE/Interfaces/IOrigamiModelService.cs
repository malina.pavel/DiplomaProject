﻿using DIYAppBE.Models;
using DIYAppBE.Models.DTOs;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Interfaces
{
    public interface IOrigamiModelService
    {
        Task<List<OrigamiModel>> GetOrigamis();
        Task<OrigamiWithStepsDTO> GetOrigami(string slug);
        Task<IList<OrigamiModel>> GetOrigamisByString(string search);
        Task AddOrigami(OrigamiModel origami);
        Task UpdateOrigami(Guid origamiId, JsonPatchDocument<OrigamiModel> patch);
        Task DeleteOrigami(Guid origamiId);
    }
}
