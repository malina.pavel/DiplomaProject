﻿using Microsoft.Azure.CognitiveServices.Vision.CustomVision.Prediction;
using Microsoft.Azure.CognitiveServices.Vision.CustomVision.Training.Models;
using Microsoft.Azure.CognitiveServices.Vision.CustomVision.Training;

namespace DIYAppBE.Interfaces
{
    public interface ITrainingService
    {
        Task<CustomVisionTrainingClient> AuthenticateTraining();
        Task<CustomVisionPredictionClient> AuthenticatePrediction();
        Task<Project> CreateProject(string projectName);
        Task<Project> GetProjectById(Guid projectId);
        Task<Guid> CreateTag(Guid projectId, string tagName);
        Task UploadTrainingImages(Guid projectId, List<IFormFile> imageFiles, Guid tagId);
        Task TrainProject(Guid projectId);
        Task PublishCurrentIteration(Guid projectId);
    }
}
