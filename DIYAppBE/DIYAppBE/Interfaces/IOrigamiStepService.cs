﻿using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Interfaces
{
    public interface IOrigamiStepService
    {
        Task<IList<OrigamiStep>> GetSteps(Guid origamiId);
        Task AddStep(OrigamiStep origamiStep);
        Task UpdateStep(Guid origamiId, JsonPatchDocument<OrigamiStep> patch);
        Task DeleteStep(Guid origamiId);
    }
}
