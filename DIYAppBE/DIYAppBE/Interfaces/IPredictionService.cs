﻿using DIYAppBE.Models;
using Microsoft.Azure.CognitiveServices.Vision.CustomVision.Prediction;
using Microsoft.Azure.CognitiveServices.Vision.CustomVision.Training;

namespace DIYAppBE.Interfaces
{
    public interface IPredictionService
    {
        Task<PredictionResult> PredictImageAsync(string imageUrl);
        Task<CustomVisionTrainingClient> AuthenticateTraining();
        Task<CustomVisionPredictionClient> AuthenticatePrediction();
        Task<Microsoft.Azure.CognitiveServices.Vision.CustomVision.Prediction.Models.ImagePrediction> PredictResult(Guid projectId, string imageUrl);
    }
}
