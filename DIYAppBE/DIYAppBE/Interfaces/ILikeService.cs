﻿using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Interfaces
{
    public interface ILikeService
    {
        Task<IList<Like>> GetLikes(Guid postId);
        Task AddLike(Like like);
        Task DeleteLike(Guid userId, Guid postId);
    }
}
