﻿using DIYAppBE.Models;
using DIYAppBE.Models.DTOs;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Interfaces
{
    public interface IArticleService
    {
        Task<List<Article>> GetArticles();
        Task<List<Article>> GetPublishedArticles();
        Task<ArticleWithStepsDTO> GetArticle(string slug);
        Task<IList<Article>> GetArticlesByString(string search);
        Task<IList<Article>> GetDraftsByUser(Guid userId, bool trackChanges);
        Task AddArticle(Article article);
        Task UpdateDraftArticle(Guid articleId, JsonPatchDocument<Article> patch);
        Task DeleteDraftArticle(Guid articleId);
    }
}
