﻿using DIYAppBE.Models;

namespace DIYAppBE.Interfaces
{
    public interface IJWTService
    {
        string GenerateToken(User user);
    }
}
