﻿using DIYAppBE.Models;
using DIYAppBE.Models.DTOs;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Interfaces
{
    public interface IPostService
    {
        Task<List<Post>> GetPosts();
        Task<PostWithMetricsDTO> GetPost(string slug);
        Task<IList<Post>> GetPostsByString(string search);
        Task AddPost(Post post);
        Task UpdatePost(Guid postId, JsonPatchDocument<Post> patch);
        Task DeletePost(Guid postId);
    }
}
