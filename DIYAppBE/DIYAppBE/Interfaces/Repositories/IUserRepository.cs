﻿using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Interfaces.Repositories
{
    public interface IUserRepository
    {
        Task<User> GetUser(Guid userId);
        Task<IEnumerable<User>> GetUsers(bool trackChanges);
        Task<User> GetUserByString(string str);
        Task CreateUser(User user);
        Task UpdateUser(Guid userId, JsonPatchDocument<User> patch);
        Task DeleteUser(Guid userId);
    }
}
