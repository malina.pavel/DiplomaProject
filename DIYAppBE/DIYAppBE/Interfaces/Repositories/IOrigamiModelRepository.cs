﻿using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Interfaces.Repositories
{
    public interface IOrigamiModelRepository
    {
        Task<OrigamiModel> GetOrigami(string slug);
        Task<IEnumerable<OrigamiModel>> GetOrigamis(bool trackChanges);
        Task<IEnumerable<OrigamiModel>> GetOrigamisByString(string search, bool trackChanges);
        Task CreateOrigami(OrigamiModel origami);
        Task UpdateOrigami(Guid origamiId, JsonPatchDocument<OrigamiModel> patch);
        Task DeleteOrigami(Guid origamiId);
    }
}
