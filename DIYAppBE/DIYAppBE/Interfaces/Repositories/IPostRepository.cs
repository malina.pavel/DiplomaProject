﻿using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Interfaces.Repositories
{
    public interface IPostRepository
    {
        Task<Post> GetPost(string slug);
        Task<IEnumerable<Post>> GetPosts(bool trackChanges);
        Task<IEnumerable<Post>> GetPostsByString(string search, bool trackChanges);
        Task CreatePost(Post post);
        Task UpdatePost(Guid postId, JsonPatchDocument<Post> patch);
        Task DeletePost(Guid postId);
    }
}
