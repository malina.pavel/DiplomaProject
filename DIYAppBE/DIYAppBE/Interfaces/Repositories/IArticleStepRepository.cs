﻿using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Interfaces.Repositories
{
    public interface IArticleStepRepository
    {
        Task<ArticleStep> GetArticleStep(Guid stepId);
        Task<IEnumerable<ArticleStep>> GetArticleSteps(bool trackChanges);
        Task<IEnumerable<ArticleStep>> GetStepsFromArticle(Guid articleId, bool trackChanges);
        Task CreateArticleStep(ArticleStep articleStep);
        Task UpdateArticleStep(Guid stepId, JsonPatchDocument<ArticleStep> patch);
        Task DeleteArticleStep(Guid stepId);
    }
}
