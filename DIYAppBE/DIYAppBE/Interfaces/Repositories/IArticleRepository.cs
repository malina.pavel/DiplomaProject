﻿using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Interfaces.Repositories
{
    public interface IArticleRepository
    {
        Task<Article> GetArticle(string slug);
        Task<IEnumerable<Article>> GetArticles(bool trackChanges);
        Task<IEnumerable<Article>> GetPublishedArticles(bool trackChanges);
        Task<IEnumerable<Article>> GetArticlesByString(string search, bool trackChanges);
        Task<IEnumerable<Article>> GetDraftsByUser(Guid userId, bool trackChanges);
        Task CreateArticle(Article article);
        Task UpdateDraftArticle(Guid articleId, JsonPatchDocument<Article> patch);
        Task DeleteDraftArticle(Guid articleId);
    }
}
