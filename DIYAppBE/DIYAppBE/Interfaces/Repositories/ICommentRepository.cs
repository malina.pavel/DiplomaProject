﻿using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Interfaces.Repositories
{
    public interface ICommentRepository
    {
        Task<Comment> GetComment(Guid commentId);
        Task<IEnumerable<Comment>> GetComments(bool trackChanges);
        Task<IEnumerable<Comment>> GetCommentsByPost(Guid postId, bool trackChanges);
        Task CreateComment(Comment comment);
        Task UpdateComment(Guid commentId, JsonPatchDocument<Comment> patch);
        Task DeleteComment(Guid commentId);
    }
}
