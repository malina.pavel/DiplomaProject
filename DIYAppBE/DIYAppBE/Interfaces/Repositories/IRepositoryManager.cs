﻿namespace DIYAppBE.Interfaces.Repositories
{
    public interface IRepositoryManager
    {
        IUserRepository Users { get; }
        IArticleRepository Articles { get; }
        IArticleStepRepository ArticleSteps { get; }
        IPostRepository Posts { get; }
        ICommentRepository Comments { get; }
        ILikeRepository Likes { get; }
        IOrigamiModelRepository Origamis { get; }
        IOrigamiStepRepository OrigamiSteps { get; }
        Task Save();
    }
}
