﻿using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Interfaces.Repositories
{
    public interface IOrigamiStepRepository
    {
        Task<OrigamiStep> GetOrigamiStep(Guid stepId);
        Task<IEnumerable<OrigamiStep>> GetOrigamiSteps(bool trackChanges);
        Task<IEnumerable<OrigamiStep>> GetStepsFromOrigami(Guid origamiId, bool trackChanges);
        Task CreateOrigamiStep(OrigamiStep origamiStep);
        Task UpdateOrigamiStep(Guid stepId, JsonPatchDocument<OrigamiStep> patch);
        Task DeleteOrigamiStep(Guid stepId);
    }
}
