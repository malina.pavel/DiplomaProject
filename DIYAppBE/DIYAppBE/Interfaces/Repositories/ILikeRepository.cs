﻿using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Interfaces.Repositories
{
    public interface ILikeRepository
    {
        Task<Like> GetLike(Guid likeId);
        Task<IEnumerable<Like>> GetLikes(bool trackChanges);
        Task<IEnumerable<Like>> GetLikesByPost(Guid postId, bool trackChanges);
        Task CreateLike(Like like);
        Task DeleteLike(Guid userId, Guid postId);
    }
}
