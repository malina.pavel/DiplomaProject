﻿using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Interfaces
{
    public interface ICommentService
    {
        Task<IList<Comment>> GetComments(Guid postId);
        Task AddComment(Comment comment);
        Task UpdateComment(Guid commentId, JsonPatchDocument<Comment> patch);
        Task DeleteComment(Guid commentId);
    }
}
