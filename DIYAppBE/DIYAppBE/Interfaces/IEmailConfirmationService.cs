﻿using DIYAppBE.Models;

namespace DIYAppBE.Interfaces
{
    public interface IEmailConfirmationService
    {
        Task<bool> SendEmailConfirmationAsync(EmailConfirmation email);
        EmailConfirmation ComposeEmail(User user, string emailType);
        string GenerateEmailToken();
    }
}
