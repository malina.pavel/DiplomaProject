﻿using DIYAppBE.Models;
using DIYAppBE.Models.DTOs;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Interfaces
{
    public interface IUserService
    {
        Task<List<User>> GetUsers();
        Task<UserSessionDTO> GetUser(Guid userId);
        Task<User> GetUserByString(string str);
        Task AddUser(User user);
        Task UpdateUser(Guid userId, JsonPatchDocument<User> patch);
        Task DeleteUser(Guid userId);
    }
}
