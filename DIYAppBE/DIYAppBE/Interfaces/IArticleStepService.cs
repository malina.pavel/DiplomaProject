﻿using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Interfaces
{
    public interface IArticleStepService
    {
        Task<IList<ArticleStep>> GetSteps(Guid articleId);
        Task AddStep(ArticleStep articleStep);
        Task UpdateStep(Guid articleId, JsonPatchDocument<ArticleStep> patch);
        Task DeleteStep(Guid articleId);
    }
}
