﻿using DIYAppBE.Models;
using DIYAppBE.Models.DTOs;

namespace DIYAppBE.Interfaces
{
    public interface IUserSigningService
    {
        Task RegisterUser(User user);
        Task<bool> ValidateUser(UserLoginDTO loginAttempt);
    }
}
