﻿using DIYAppBE.Data;
using DIYAppBE.Interfaces.Repositories;

namespace DIYAppBE.Repositories
{
    public class RepositoryManager : IRepositoryManager
    {
        private ApplicationDbContext _applicationDbContext;
        private IUserRepository? _userRepository;
        private IArticleRepository? _articleRepository;
        private IArticleStepRepository? _articleStepRepository;
        private IPostRepository? _postRepository;
        private ICommentRepository? _commentRepository;
        private ILikeRepository? _likeRepository;
        private IOrigamiModelRepository? _origamiRepository;
        private IOrigamiStepRepository? _origamiStepRepository;

        public RepositoryManager(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public IUserRepository Users
        {
            get
            {
                if( _userRepository == null ) 
                { 
                    _userRepository = new UserRepository( _applicationDbContext );
                }
                return _userRepository;
            }
        }

        public IArticleRepository Articles
        {
            get
            {
                if( _articleRepository == null )
                {
                    _articleRepository = new ArticleRepository( _applicationDbContext );
                }
                return _articleRepository;
            }
        }

        public IArticleStepRepository ArticleSteps
        {
            get
            {
                if( _articleStepRepository == null )
                {
                    _articleStepRepository = new ArticleStepRepository( _applicationDbContext );
                }
                return _articleStepRepository;
            }
        }

        public IPostRepository Posts
        {
            get 
            { 
                if(_postRepository == null)
                {
                    _postRepository = new PostRepository(_applicationDbContext );
                }
                return _postRepository; 
            }
        }

        public ICommentRepository Comments
        {
            get
            {
                if (_commentRepository == null)
                {
                    _commentRepository = new CommentRepository(_applicationDbContext);
                }
                return _commentRepository;
            }
        }

        public IOrigamiModelRepository Origamis
        {
            get
            {
                if( _origamiRepository == null )
                {
                    _origamiRepository = new OrigamiModelRepository( _applicationDbContext );
                }
                return _origamiRepository;
            }
        }

        public IOrigamiStepRepository OrigamiSteps
        {
            get
            {
                if(_origamiStepRepository == null)
                {
                    _origamiStepRepository = new OrigamiStepRepository( _applicationDbContext );
                }
                return _origamiStepRepository;
            }
        }

        public ILikeRepository Likes
        {
            get
            {
                if (_likeRepository == null)
                {
                    _likeRepository = new LikeRepository(_applicationDbContext);
                }
                return _likeRepository;
            }
        }

        public async Task Save() => await _applicationDbContext.SaveChangesAsync();
    }
}
