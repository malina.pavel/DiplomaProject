﻿using DIYAppBE.Data;
using DIYAppBE.Interfaces.Repositories;
using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;

namespace DIYAppBE.Repositories
{
    public class ArticleStepRepository : RepositoryBase<ArticleStep>, IArticleStepRepository
    {
        public ArticleStepRepository(ApplicationDbContext _ApplicationDbContext) : base(_ApplicationDbContext) { }

        public async Task CreateArticleStep(ArticleStep articleStep)
        {
            articleStep.Id = Guid.NewGuid();
            await Create(articleStep);
        }

        public async Task<ArticleStep> GetArticleStep(Guid stepId) => await FindByCondition(x => x.Id == stepId, false).FirstAsync();

        public async Task<IEnumerable<ArticleStep>> GetArticleSteps(bool trackChanges) => await FindAll(trackChanges).ToListAsync();

        public async Task<IEnumerable<ArticleStep>> GetStepsFromArticle(Guid articleId, bool trackChanges) => await FindByCondition(x => x.ArticleId == articleId, false).ToListAsync();

        public async Task UpdateArticleStep(Guid stepId, JsonPatchDocument<ArticleStep> patch)
        {
            var step = await FindByCondition(x => x.Id == stepId, false).FirstAsync();
            if (step != null)
            {
                patch.ApplyTo(step);
                await Task.Run(() => Update(step));
            }
        }

        public async Task DeleteArticleStep(Guid stepId)
        {
            var step = await FindByCondition(x => x.Id == stepId, false).FirstAsync();
            if (step != null)
            {
                await Task.Run(() => Delete(step));
            }
        }
    }
}
