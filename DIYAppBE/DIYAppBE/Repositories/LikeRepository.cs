﻿using DIYAppBE.Data;
using DIYAppBE.Interfaces.Repositories;
using DIYAppBE.Models;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.Design;

namespace DIYAppBE.Repositories
{
    public class LikeRepository : RepositoryBase<Like>, ILikeRepository
    {
        public LikeRepository(ApplicationDbContext _ApplicationDbContext) : base(_ApplicationDbContext) { }

        public async Task CreateLike(Like like)
        {
            like.Id = Guid.NewGuid();
            await Create(like);
        }

        public async Task<Like> GetLike(Guid likeId) => await FindByCondition(x => x.Id == likeId, true).FirstAsync();

        public async Task<IEnumerable<Like>> GetLikes(bool trackChanges) => await FindAll(trackChanges).ToListAsync();

        public async Task<IEnumerable<Like>> GetLikesByPost(Guid postId, bool trackChanges) => await FindByCondition(x => x.PostId == postId, trackChanges).ToListAsync();

        public async Task DeleteLike(Guid userId, Guid postId)
        {
            var likeByUser = await FindByCondition(x => x.UserId == userId && x.PostId == postId, false).FirstAsync();
            if (likeByUser != null)
            {
                await Task.Run(() => Delete(likeByUser));
            }
        }
    }
}
