﻿using DIYAppBE.Data;
using DIYAppBE.Interfaces.Repositories;
using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;

namespace DIYAppBE.Repositories
{
    public class OrigamiStepRepository : RepositoryBase<OrigamiStep>, IOrigamiStepRepository
    {
        public OrigamiStepRepository(ApplicationDbContext _ApplicationDbContext) : base(_ApplicationDbContext) { }

        public async Task CreateOrigamiStep(OrigamiStep origamiStep)
        {
            origamiStep.Id = Guid.NewGuid();
            await Create(origamiStep);
        }

        public async Task<OrigamiStep> GetOrigamiStep(Guid stepId) => await FindByCondition(x => x.Id == stepId, false).FirstAsync();

        public async Task<IEnumerable<OrigamiStep>> GetOrigamiSteps(bool trackChanges) => await FindAll(trackChanges).ToListAsync();

        public async Task<IEnumerable<OrigamiStep>> GetStepsFromOrigami(Guid origamiId, bool trackChanges) => await FindByCondition(x => x.OrigamiId == origamiId, false).ToListAsync();

        public async Task UpdateOrigamiStep(Guid stepId, JsonPatchDocument<OrigamiStep> patch)
        {
            var step = await FindByCondition(x => x.Id == stepId, false).FirstAsync();
            if (step != null)
            {
                patch.ApplyTo(step);
                await Task.Run(() => Update(step));
            }
        }

        public async Task DeleteOrigamiStep(Guid stepId)
        {
            var step = await FindByCondition(x => x.Id == stepId, false).FirstAsync();
            if (step != null)
            {
                await Task.Run(() => Delete(step));
            }
        }
    }
}
