﻿using DIYAppBE.Data;
using DIYAppBE.Interfaces.Repositories;
using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using static DIYAppBE.Helpers.SlugGenerator;

namespace DIYAppBE.Repositories
{
    public class OrigamiModelRepository : RepositoryBase<OrigamiModel>, IOrigamiModelRepository
    {
        public OrigamiModelRepository(ApplicationDbContext _ApplicationDbContext) : base(_ApplicationDbContext) { }

        public async Task CreateOrigami(OrigamiModel origami)
        {
            // initialize with default values
            origami.Id = Guid.NewGuid();
            origami.Slug = GenerateUniqueSlugIfDuplicates(origami);

            await Create(origami);
        }

        public async Task<OrigamiModel> GetOrigami(string slug) => await FindByCondition(x => x.Slug == slug, false).FirstAsync();
        
        public async Task<IEnumerable<OrigamiModel>> GetOrigamis(bool trackChanges) => await FindAll(trackChanges).ToListAsync();

        public async Task<IEnumerable<OrigamiModel>> GetOrigamisByString(string search, bool trackChanges) => await FindByCondition(x => x.Name.Contains(search), trackChanges).ToListAsync();

        public async Task UpdateOrigami(Guid origamiId, JsonPatchDocument<OrigamiModel> patch)
        {
            var origami = await FindByCondition(x => x.Id == origamiId, false).FirstAsync();
            if (origami != null)
            {
                patch.ApplyTo(origami);
                if (patch.Operations.Any(op => op.path.Equals("/name")))
                {
                    origami.Slug = GenerateUniqueSlugIfDuplicates(origami);
                }
                await Task.Run(() => Update(origami));
            }
        }

        public async Task DeleteOrigami(Guid origamiId)
        {
            var draft = FindByCondition(x => x.Id == origamiId, false).First();
            if (draft != null)
            {
                await Task.Run(() => Delete(draft));
            }
        }


        // in case there are duplicate slugs
        private string GenerateUniqueSlugIfDuplicates(OrigamiModel origami)
        {
            origami.Slug = GenerateInitialSlug(origami.Name);
            var identicalSlugsCount = FindByCondition(x => x.Slug.Contains(origami.Slug) && x.Id != origami.Id, false).Count();
            // if there are more than one identical slugs
            if (identicalSlugsCount > 1)
            {
                var lastSlug = FindByCondition(x => x.Slug.Contains(origami.Slug) && x.Id != origami.Id, false).OrderByDescending(x => x.Slug).First();
                var lastSlugCount = lastSlug.Slug.Last() - '0';
                if (lastSlugCount >= identicalSlugsCount) identicalSlugsCount = lastSlugCount + 1;
            }

            return GenerateUniqueSlug(origami.Slug, identicalSlugsCount);
        }
    }
}
