﻿using DIYAppBE.Data;
using DIYAppBE.Interfaces.Repositories;
using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using static DIYAppBE.Helpers.SlugGenerator;

namespace DIYAppBE.Repositories
{
    public class ArticleRepository : RepositoryBase<Article>, IArticleRepository
    {
        public ArticleRepository(ApplicationDbContext _ApplicationDbContext) : base(_ApplicationDbContext) { }


        public async Task CreateArticle(Article article)
        {
            // initialize with default values
            article.Id = Guid.NewGuid();
            article.CreatedDate = article.PublishedDate = DateTime.UtcNow;
            article.Views = 0;
            article.IsPublished = false; // initially, an article is a draft
            article.Slug = GenerateUniqueSlugIfDuplicates(article);

            await Create(article);
        }

        public async Task<Article> GetArticle(string slug) => await FindByCondition(x => x.Slug == slug, true).FirstAsync();

        public async Task<IEnumerable<Article>> GetArticles(bool trackChanges) => await FindAll(trackChanges).ToListAsync();

        public async Task<IEnumerable<Article>> GetPublishedArticles(bool trackChanges) => await FindByCondition(x => x.IsPublished == true, trackChanges).ToListAsync();

        public async Task<IEnumerable<Article>> GetArticlesByString(string search, bool trackChanges) => await FindByCondition(x => x.Title.Contains(search), trackChanges).ToListAsync();
        
        public async Task<IEnumerable<Article>> GetDraftsByUser(Guid userId, bool trackChanges) => await FindByCondition(x => x.UserId == userId && x.IsPublished == false, trackChanges).ToListAsync();
        
        public async Task UpdateDraftArticle(Guid articleId, JsonPatchDocument<Article> patch)
        {
            var draft = await FindByCondition(x => x.Id == articleId && x.IsPublished == false, false).FirstAsync();
            if (draft != null)
            {
                patch.ApplyTo(draft);
                if (patch.Operations.Any(op => op.path.Equals("/title")))
                {
                    draft.Slug = GenerateUniqueSlugIfDuplicates(draft);
                }
                if (patch.Operations.Any(op => op.path.Equals("/isPublished") && op.value.ToString().ToLower() == "true"))
                {
                    draft.PublishedDate = DateTime.UtcNow;
                }
                await Task.Run(() => Update(draft));
            }
        }

        public async Task DeleteDraftArticle(Guid articleId)
        {
            var draft = FindByCondition(x => x.Id == articleId && x.IsPublished == false, false).First();
            if (draft != null)
            {
                await Task.Run(() => Delete(draft));
            }
        }


        // in case there are duplicate slugs
        private string GenerateUniqueSlugIfDuplicates(Article draftArticle)
        {
            draftArticle.Slug = GenerateInitialSlug(draftArticle.Title);
            var identicalSlugsCount = FindByCondition(x => x.Slug.Contains(draftArticle.Slug) && x.Id != draftArticle.Id, false).Count();
            // if there are more than one identical slugs
            if (identicalSlugsCount > 1)
            {
                var lastSlug = FindByCondition(x => x.Slug.Contains(draftArticle.Slug) && x.Id != draftArticle.Id, false).OrderByDescending(x => x.Slug).First();
                var lastSlugCount = lastSlug.Slug.Last() - '0';
                if (lastSlugCount >= identicalSlugsCount) identicalSlugsCount = lastSlugCount + 1;
            }

            return GenerateUniqueSlug(draftArticle.Slug, identicalSlugsCount);
        }
    }
}
