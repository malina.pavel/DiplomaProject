﻿using DIYAppBE.Data;
using DIYAppBE.Interfaces.Repositories;
using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using static DIYAppBE.Helpers.SlugGenerator;

namespace DIYAppBE.Repositories
{
    public class PostRepository : RepositoryBase<Post>, IPostRepository
    {
        public PostRepository(ApplicationDbContext _ApplicationDbContext) : base(_ApplicationDbContext) { }

        public async Task CreatePost(Post post)
        {
            // initialize with default values
            post.Id = Guid.NewGuid();
            post.CreatedDate = DateTime.UtcNow;
            post.Likes = 0;
            post.Slug = GenerateUniqueSlugIfDuplicates(post);
            await Create(post);
        }

        public async Task<Post> GetPost(string slug) => await FindByCondition(x => x.Slug == slug, false).FirstAsync();

        public async Task<IEnumerable<Post>> GetPosts(bool trackChanges) => await FindAll(trackChanges).ToListAsync();

        public async Task<IEnumerable<Post>> GetPostsByString(string search, bool trackChanges) => await FindByCondition(x => x.Title.Contains(search), trackChanges).ToListAsync();

        public async Task UpdatePost(Guid postId, JsonPatchDocument<Post> patch)
        {
            var post = await FindByCondition(x => x.Id == postId, false).FirstAsync();
            if (post != null)
            {
                patch.ApplyTo(post);
                if (patch.Operations.Any(op => op.path.Equals("/title")))
                {
                    post.Slug = GenerateUniqueSlugIfDuplicates(post);
                }
                await Task.Run(() => Update(post));
            }
        }

        public async Task DeletePost(Guid postId)
        {
            var post = FindByCondition(x => x.Id == postId, false).First();
            if (post != null)
            {
                await Task.Run(() => Delete(post));
            }
        }


        // in case there are duplicate slugs
        private string GenerateUniqueSlugIfDuplicates(Post post)
        {
            post.Slug = GenerateInitialSlug(post.Title);
            var identicalSlugsCount = FindByCondition(x => x.Slug.Contains(post.Slug) && x.Id != post.Id, false).Count();
            // if there are more than one identical slugs
            if (identicalSlugsCount > 1)
            {
                var lastSlug = FindByCondition(x => x.Slug.Contains(post.Slug) && x.Id != post.Id, false).OrderByDescending(x => x.Slug).First();
                var lastSlugCount = lastSlug.Slug.Last() - '0';
                if (lastSlugCount >= identicalSlugsCount) identicalSlugsCount = lastSlugCount + 1;
            }

            return GenerateUniqueSlug(post.Slug, identicalSlugsCount);
        }
    }
}
