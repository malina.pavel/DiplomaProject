﻿using DIYAppBE.Data;
using DIYAppBE.Interfaces.Repositories;
using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using static DIYAppBE.Helpers.PasswordHasher;

namespace DIYAppBE.Repositories
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(ApplicationDbContext _ApplicationDbContext) : base(_ApplicationDbContext) { }

        public async Task CreateUser(User user) => await Create(user);

        public async Task<User> GetUser(Guid userId) => await FindByCondition(x => x.Id == userId, false).FirstAsync();

        public async Task<IEnumerable<User>> GetUsers(bool trackChanges) => await FindAll(trackChanges).ToListAsync();

        public async Task<User> GetUserByString(string str) => await FindByCondition(x => x.Username == str, false).FirstAsync();

        public async Task UpdateUser(Guid userId, JsonPatchDocument<User> patch)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();

            var user = await FindByCondition(x => x.Id == userId, false).FirstAsync();
            if (user != null)
            {
                patch.ApplyTo(user);
                if (patch.Operations.Any(op => op.path.Equals("/password")))
                {
                    user.PasswordSalt = GenerateSalt();
                    user.Password = ComputeHash(
                        user.Password,
                        user.PasswordSalt,
                        configuration["Hashing:Pepper"],
                        Int32.Parse(configuration["Hashing:Iterations"])
                    );
                }
                await Task.Run(() => Update(user));
            }
        }

        public async Task DeleteUser(Guid userId)
        {
            var user = await FindByCondition(x => x.Id == userId, false).FirstAsync();
            if(user != null)
            {
                await Task.Run(() => Delete(user));
            }
        }
    }
}
