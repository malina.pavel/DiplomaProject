﻿using DIYAppBE.Data;
using DIYAppBE.Interfaces.Repositories;
using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;

namespace DIYAppBE.Repositories
{
    public class CommentRepository : RepositoryBase<Comment>, ICommentRepository
    {
        public CommentRepository(ApplicationDbContext _ApplicationDbContext) : base(_ApplicationDbContext) { }

        public async Task CreateComment(Comment comment)
        {
            // initialize with default values
            comment.Id = Guid.NewGuid();
            comment.CreatedDate = DateTime.UtcNow;

            await Create(comment);
        }

        public async Task<Comment> GetComment(Guid commentId) => await FindByCondition(x => x.Id == commentId, true).FirstAsync();

        public async Task<IEnumerable<Comment>> GetComments(bool trackChanges) => await FindAll(trackChanges).ToListAsync();

        public async Task<IEnumerable<Comment>> GetCommentsByPost(Guid postId, bool trackChanges) => await FindByCondition(x => x.PostId == postId, trackChanges).ToListAsync();

        public async Task UpdateComment(Guid commentId, JsonPatchDocument<Comment> patch)
        {
            var comment = await FindByCondition(x => x.Id == commentId, false).FirstAsync();
            if (comment != null)
            {
                patch.ApplyTo(comment);
                await Task.Run(() => Update(comment));
            }
        }

        public async Task DeleteComment(Guid commentId)
        {
            var comment = await FindByCondition(x => x.Id == commentId, false).FirstAsync();
            if (comment != null)
            {
                await Task.Run(() => Delete(comment));
            }
        }
    }
}
