﻿namespace DIYAppBE.Helpers
{
    public class SlugGenerator
    {
        public static string GenerateInitialSlug(string title)
        {
            return new string(
                title.ToLower()
                     .Replace(' ', '-')
                     .Replace(",", "")
                     .Replace(";", "")
                     .Replace(":", "")
                     .Where(c => Char.IsLetterOrDigit(c) || c == '-')
                     .ToArray()
            );
        }

        public static string GenerateUniqueSlug(string slug, int identicalSlugsCount)
        {
            if (identicalSlugsCount != 0)
            {
                slug += "-" + identicalSlugsCount.ToString();
            }
            return slug;
        }
    }
}
