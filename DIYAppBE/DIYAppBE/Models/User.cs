﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DIYAppBE.Models
{
    public class User
    {
        public Guid Id { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public string? ProfilePicture { get; set; }
        public string UserRole { get; set; }
        public string EmailToken { get; set; }
        public bool EmailConfirmed { get; set; }
    }
}
