﻿using System.ComponentModel.DataAnnotations;

namespace DIYAppBE.Models
{
    public class OrigamiModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Overview { get; set; }
        [Required]
        public string Thumbnail { get; set; }
        public string Slug { get; set; }
    }
}
