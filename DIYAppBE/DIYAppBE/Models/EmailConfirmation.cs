﻿namespace DIYAppBE.Models
{
    public class EmailConfirmation
    {
        public string To { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
    }
}
