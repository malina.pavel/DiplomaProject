﻿using System.ComponentModel.DataAnnotations;

namespace DIYAppBE.Models
{
    public class OrigamiStep
    {
        public Guid Id { get; set; }
        public Guid OrigamiId { get; set; }
        public int StepNumber { get; set; }
        public string Description { get; set; }
        [Required]
        public string ImageURL { get; set; }
    }
}
