﻿using System.ComponentModel.DataAnnotations;

namespace DIYAppBE.Models
{
    public class Article
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }    
        [Required]
        public string Title { get; set; }
        [Required]
        public string Overview { get; set; }
        [Required]
        public string Category { get; set; }
        [Required]
        public string Thumbnail { get; set; }
        public int Views { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime PublishedDate { get; set; }
        public string Slug { get; set; }
        public bool IsPublished { get; set; } 
    }
}
