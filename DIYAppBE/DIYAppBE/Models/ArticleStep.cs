﻿using System.ComponentModel.DataAnnotations;

namespace DIYAppBE.Models
{
    public class ArticleStep
    {
        public Guid Id { get; set; }
        public Guid ArticleId { get; set; }
        public int StepNumber { get; set; }
        public string Title { get; set; }
        [Required]
        public string Overview { get; set; }
        public string? ImageURL { get; set; }
    }
}
