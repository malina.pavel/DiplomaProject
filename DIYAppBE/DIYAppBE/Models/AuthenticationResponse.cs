﻿namespace DIYAppBE.Models
{
    public class AuthenticationResponse
    {
        public string? Token { get; set; }
    }
}
