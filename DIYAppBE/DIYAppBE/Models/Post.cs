﻿using System.ComponentModel.DataAnnotations;

namespace DIYAppBE.Models
{
    public class Post
    {
        public Guid Id { get; set; } 
        public Guid UserId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        public string Category { get; set; }
        public string ImageURL { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Likes { get; set; }
        public string Slug { get; set; }
    }
}
