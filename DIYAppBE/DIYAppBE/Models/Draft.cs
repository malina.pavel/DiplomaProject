﻿namespace DIYAppBE.Models
{
    public class Draft
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid PostId { get; set; }
    }
}
