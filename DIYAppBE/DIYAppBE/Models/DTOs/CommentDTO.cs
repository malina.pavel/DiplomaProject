﻿using System.ComponentModel.DataAnnotations;

namespace DIYAppBE.Models.DTOs
{
    public class CommentDTO
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        [Required]
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
