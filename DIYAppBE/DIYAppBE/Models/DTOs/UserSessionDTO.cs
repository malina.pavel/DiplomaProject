﻿using System.ComponentModel.DataAnnotations;

namespace DIYAppBE.Models.DTOs
{
    public class UserSessionDTO
    {
        public Guid Id { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        public string? ProfilePicture { get; set; }
        public string UserRole { get; set; }
    }
}
