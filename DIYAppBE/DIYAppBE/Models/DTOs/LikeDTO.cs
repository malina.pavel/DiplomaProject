﻿namespace DIYAppBE.Models.DTOs
{
    public class LikeDTO
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
    }
}
