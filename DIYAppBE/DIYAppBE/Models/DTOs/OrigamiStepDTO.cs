﻿using System.ComponentModel.DataAnnotations;

namespace DIYAppBE.Models.DTOs
{
    public class OrigamiStepDTO
    {
        public Guid Id { get; set; }
        public int StepNumber { get; set; }
        public string Description { get; set; }
        [Required]
        public string ImageURL { get; set; }
    }
}
