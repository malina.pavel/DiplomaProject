﻿using AutoMapper;
using DIYAppBE.Data;
using DIYAppBE.Interfaces;
using DIYAppBE.Interfaces.Repositories;
using DIYAppBE.Models;
using DIYAppBE.Models.DTOs;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.Extensions.Hosting;
using static DIYAppBE.Helpers.SlugGenerator;

namespace DIYAppBE.Services
{
    public class ArticleService : IArticleService
    {
        private readonly IRepositoryManager _repository;
        private readonly IMapper _mapper;

        public ArticleService(IRepositoryManager repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task AddArticle(Article article)
        {
            await _repository.Articles.CreateArticle(article);
            await _repository.Save();
        }

        public async Task<ArticleWithStepsDTO> GetArticle(string slug)
        {
            var article = await _repository.Articles.GetArticle(slug);
            if(article != null)
            {
                article.Views += 1;
                await _repository.Save();
            }

            var steps = await _repository.ArticleSteps.GetStepsFromArticle(article!.Id, false);
            var stepsDto = _mapper.Map<List<ArticleStepDTO>>(steps.ToList());

            var articleWithStepsDto = _mapper.Map<ArticleWithStepsDTO>(article);
            articleWithStepsDto.Steps = stepsDto;

            return articleWithStepsDto;
        }

        public async Task<List<Article>> GetArticles()
        {
            return (await _repository.Articles.GetArticles(false)).ToList();
        }

        public async Task<List<Article>> GetPublishedArticles()
        {
            return (await _repository.Articles.GetPublishedArticles(false)).ToList();
        }

        public async Task<IList<Article>> GetArticlesByString(string search)
        {
            return (await _repository.Articles.GetArticlesByString(search, false)).ToList();
        }        
        
        public async Task<IList<Article>> GetDraftsByUser(Guid userId, bool trackChanges)
        {
            return (await _repository.Articles.GetDraftsByUser(userId, trackChanges)).ToList();
        }

        public async Task UpdateDraftArticle(Guid articleId, JsonPatchDocument<Article> patch)
        {
            await _repository.Articles.UpdateDraftArticle(articleId, patch);
            await _repository.Save();
        }

        public async Task DeleteDraftArticle(Guid articleId)
        {
            await _repository.Articles.DeleteDraftArticle(articleId);
            await _repository.Save();
        }
    }
}
