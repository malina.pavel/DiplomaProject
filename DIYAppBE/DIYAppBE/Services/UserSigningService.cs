﻿using DIYAppBE.Interfaces;
using DIYAppBE.Interfaces.Repositories;
using DIYAppBE.Models;
using DIYAppBE.Models.DTOs;
using static DIYAppBE.Helpers.PasswordHasher;
using static System.Net.WebRequestMethods;

namespace DIYAppBE.Services
{
    public class UserSigningService : IUserSigningService
    {
        private readonly IRepositoryManager _repository;
        private readonly IConfiguration _configuration;

        public UserSigningService(IRepositoryManager repository, IConfiguration configuration)
        {
            _repository = repository;
            _configuration = configuration;
        }

        public async Task RegisterUser(User user)
        {
            user.Id = Guid.NewGuid();
            user.ProfilePicture = "https://diyproject.blob.core.windows.net/diyproject/empty%20pfp.PNG";
            user.PasswordSalt = GenerateSalt();
            user.Password = ComputeHash(
                user.Password,
                user.PasswordSalt,
                _configuration["Hashing:Pepper"],
                Int32.Parse(_configuration["Hashing:Iterations"])
            );
            user.EmailConfirmed = false;
            await _repository.Users.CreateUser(user);
            await _repository.Save();
        }

        public async Task<bool> ValidateUser(UserLoginDTO loginAttempt)
        {
            var userFound = await _repository.Users.GetUserByString(loginAttempt.Username);
            if (userFound == null)  return false;
            var attemptHash = ComputeHash(
                loginAttempt.Password, 
                userFound.PasswordSalt,
                _configuration["Hashing:Pepper"],
                Int32.Parse(_configuration["Hashing:Iterations"])
            );

            return userFound.Password == attemptHash ? true : false;
        }
    }
}
