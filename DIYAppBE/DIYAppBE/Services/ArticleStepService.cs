﻿using DIYAppBE.Interfaces;
using DIYAppBE.Interfaces.Repositories;
using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Services
{
    public class ArticleStepService : IArticleStepService
    {
        private readonly IRepositoryManager _repository;

        public ArticleStepService(IRepositoryManager repository)
        {
            _repository = repository;
        }

        public async Task AddStep(ArticleStep articleStep)
        {
            await _repository.ArticleSteps.CreateArticleStep(articleStep);
            await _repository.Save();
        }

        public async Task<IList<ArticleStep>> GetSteps(Guid articleId) => (await _repository.ArticleSteps.GetStepsFromArticle(articleId, false)).ToList();

        public async Task UpdateStep(Guid articleId, JsonPatchDocument<ArticleStep> patch)
        {
            await _repository.ArticleSteps.UpdateArticleStep(articleId, patch);
            await _repository.Save();
        }

        public async Task DeleteStep(Guid articleId)
        {
            await _repository.ArticleSteps.DeleteArticleStep(articleId);
            await _repository.Save();
        }
    }
}
