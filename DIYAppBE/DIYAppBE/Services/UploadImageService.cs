﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using DIYAppBE.Interfaces;

namespace DIYAppBE.Services
{
    public class UploadImageService : IUploadImageService
    {
        private readonly string _storageConnectionString;
        public UploadImageService(IConfiguration configuration)
        {
            _storageConnectionString = configuration.GetConnectionString("AzureStorage");
        }
        public async Task<string> UploadAsync(Stream fileStream, string fileName)
        {
            var container = new BlobContainerClient(_storageConnectionString, "diyproject");
            var blob = container.GetBlobClient(fileName);
            await blob.DeleteIfExistsAsync(DeleteSnapshotsOption.IncludeSnapshots);
            await blob.UploadAsync(fileStream);
            return blob.Uri.ToString();
        }
    }
}
