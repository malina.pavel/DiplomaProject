﻿using DIYAppBE.Interfaces;
using DIYAppBE.Models;
using Microsoft.IdentityModel.Tokens;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace DIYAppBE.Services
{
    public class JWTService : IJWTService
    {
        private readonly IConfiguration _configuration;
        public JWTService(IConfiguration configuration) 
        { 
            _configuration = configuration;

        }

        public string GenerateToken(User user)
        {
            var claims = new List<Claim>()
            {
                new Claim(JwtRegisteredClaimNames.NameId, user.Id.ToString()),
                new Claim(ClaimTypes.Role, user.UserRole)
            };

            var key = GenerateSecurityKey();

            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var options = new JwtSecurityToken(
                    _configuration["JWT:Issuer"],
                    _configuration["JWT:Audience"],
                    claims,
                    expires: DateTime.Now.AddDays(1),
                    signingCredentials: credentials
            );

            var token = new JwtSecurityTokenHandler().WriteToken(options);
            return token;
        }

        private SecurityKey GenerateSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Key"]));
        }
    }
}
