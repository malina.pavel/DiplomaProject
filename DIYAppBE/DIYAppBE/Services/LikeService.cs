﻿using DIYAppBE.Interfaces;
using DIYAppBE.Interfaces.Repositories;
using DIYAppBE.Models;

namespace DIYAppBE.Services
{
    public class LikeService : ILikeService
    {
        private readonly IRepositoryManager _repository;

        public LikeService(IRepositoryManager repository)
        {
            _repository = repository;
        }

        public async Task AddLike(Like like)
        {
            await _repository.Likes.CreateLike(like);
            await _repository.Save();
        }

        public async Task<IList<Like>> GetLikes(Guid postId) => (await _repository.Likes.GetLikesByPost(postId, false)).ToList();

        public async Task DeleteLike(Guid userId, Guid postId)
        {
            await _repository.Likes.DeleteLike(userId, postId);
            await _repository.Save();
        }
    }
}
