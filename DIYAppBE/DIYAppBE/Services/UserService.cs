﻿using AutoMapper;
using DIYAppBE.Interfaces;
using DIYAppBE.Interfaces.Repositories;
using DIYAppBE.Models;
using DIYAppBE.Models.DTOs;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Services
{
    public class UserService : IUserService
    {
        private readonly IRepositoryManager _repository;
        private readonly IMapper _mapper;

        public UserService(IRepositoryManager repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task AddUser(User user)
        {
            user.Id = Guid.NewGuid();
            await _repository.Users.CreateUser(user);
            await _repository.Save();
        }

        public async Task<UserSessionDTO> GetUser(Guid userId)
        {
            var user = await _repository.Users.GetUser(userId);
            var userSessionDTO = _mapper.Map<UserSessionDTO>(user);

            return userSessionDTO;
        }

        public async Task<List<User>> GetUsers() => (await _repository.Users.GetUsers(false)).ToList();

        public async Task<User> GetUserByString(string username) => await _repository.Users.GetUserByString(username);

        public async Task UpdateUser(Guid userId, JsonPatchDocument<User> patch)
        {
            await _repository.Users.UpdateUser(userId, patch);
            await _repository.Save();
        }

        public async Task DeleteUser(Guid userId)
        {
            await _repository.Users.DeleteUser(userId);
            await _repository.Save();
        }
    }
}
