﻿using DIYAppBE.Interfaces;
using DIYAppBE.Interfaces.Repositories;
using DIYAppBE.Models;
using Mailjet.Client;
using Mailjet.Client.TransactionalEmails;
using Microsoft.AspNetCore.WebUtilities;
using System.Security.Cryptography;
using System.Text;

namespace DIYAppBE.Services
{
    public class EmailConfirmationService : IEmailConfirmationService
    {
        private readonly IConfiguration _configuration;

        public EmailConfirmationService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public EmailConfirmation ComposeEmail(User user, string emailType)
        {
            string? url;
            string? body;
            string? subject;

            switch (emailType)
            {
                case "user-registration":
                    url = $"https://localhost:4200/{_configuration["EmailConfirm:ConfirmationPath"]}?token={user.EmailToken}&email={user.Email}";

                    body = $"<img src=\"https://diyproject.blob.core.windows.net/diyproject/VulkraftLogo.png\" style=\"width:30%; height: 30%;padding-bottom: 2em\">" +
                               $"<h1 style=\"text-align: center; padding-bottom: 20px\">Welcome to Vulkraft, {user.FirstName} {user.LastName}</h1>" +
                               "<p>Activate your account by clicking the button below:</p>" +
                               $"<a href=\"{url}\" type=\"button\" style=\"background-color: #472d59; color: white; padding: 5px; border-radius: 5px; text-decoration: none\">Activate account</a>" +
                               "<p style=\"text-align: center; padding-top: 7em\">Thank you,</p>" +
                               "<p style=\"text-align: center\">Vulkraft Team</p>";

                    subject = "Activate your Vulkraft account";
                    break;

                case "forgot-password":
                    url = $"https://localhost:4200/{_configuration["EmailConfirm:PasswordRecoveryPath"]}?token={user.EmailToken}&email={user.Email}";

                    body = $"<img src=\"https://diyproject.blob.core.windows.net/diyproject/VulkraftLogo.png\" style=\"width:30%; height: 30%;padding-bottom: 2em\">" +
                               $"<h1 style=\"text-align: center; padding-bottom: 20px\">Hello, {user.FirstName} {user.LastName}</h1>" +
                               "<p>Reset your password by clicking the button below:</p>" +
                               $"<a href=\"{url}\" type=\"button\" style=\"background-color: #472d59; color: white; padding: 5px; border-radius: 5px; text-decoration: none\">Reset password</a>" +
                               "<p style=\"padding-top: 30px; color: red\">If you didn't request a password change, please ignore this mail!</p>" +
                               "<p style=\"text-align: center; padding-top: 7em\">Thank you,</p>" +
                               "<p style=\"text-align: center\">Vulkraft Team</p>";

                    subject = "Reset your password on Vulkraft";
                    break;

                default: throw new Exception("The request is unclear");
            }

            return new EmailConfirmation()
            {
                To = user.Email,
                Subject = subject,
                Body = body
            };
        }

        public async Task<bool> SendEmailConfirmationAsync(EmailConfirmation email)
        {
            var client = new MailjetClient(_configuration["Mailjet:ApiKey"], _configuration["Mailjet:SecretKey"]);

            var emailBuilder = new TransactionalEmailBuilder()
                        .WithFrom(new SendContact(_configuration["EmailConfirm:From"], _configuration["EmailConfirm:ApplicationName"]))
                        .WithTo(new SendContact(email.To))
                        .WithSubject(email.Subject)
                        .WithHtmlPart(email.Body)
                        .Build();

            var response = await client.SendTransactionalEmailAsync(emailBuilder);
            if(response.Messages != null && response.Messages[0].Status == "success")
            {
                return true;
            }

            return false;
        }

        public string GenerateEmailToken()
        {
            var token = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(GenerateEmailConfirmationToken()));
            return token;
        }

        private static string GenerateEmailConfirmationToken()
        {
            using var rng = RandomNumberGenerator.Create();
            byte[] tokenBytes = new byte[32];
            rng.GetBytes(tokenBytes);
            return Convert.ToBase64String(tokenBytes);
        }
    }
}
