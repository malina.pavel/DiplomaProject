﻿using DIYAppBE.Interfaces;
using DIYAppBE.Interfaces.Repositories;
using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Services
{
    public class CommentService : ICommentService
    {
        private readonly IRepositoryManager _repository;

        public CommentService(IRepositoryManager repository)
        {
            _repository = repository;
        }

        public async Task AddComment(Comment comment)
        {
            await _repository.Comments.CreateComment(comment);
            await _repository.Save();
        }

        public async Task<IList<Comment>> GetComments(Guid postId) => (await _repository.Comments.GetCommentsByPost(postId, false)).ToList();

        public async Task UpdateComment(Guid commentId, JsonPatchDocument<Comment> patch)
        {
            await _repository.Comments.UpdateComment(commentId, patch);
            await _repository.Save();
        }
        public async Task DeleteComment(Guid commentId)
        {
            await _repository.Comments.DeleteComment(commentId);
            await _repository.Save();
        }  
    }
}
