﻿using DIYAppBE.Interfaces;
using DIYAppBE.Interfaces.Repositories;
using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Services
{
    public class OrigamiStepService : IOrigamiStepService
    {
        private readonly IRepositoryManager _repository;

        public OrigamiStepService(IRepositoryManager repository)
        {
            _repository = repository;
        }

        public async Task AddStep(OrigamiStep origamiStep)
        {
            await _repository.OrigamiSteps.CreateOrigamiStep(origamiStep);
            await _repository.Save();
        }

        public async Task<IList<OrigamiStep>> GetSteps(Guid origamiId) => (await _repository.OrigamiSteps.GetStepsFromOrigami(origamiId, false)).ToList();

        public async Task UpdateStep(Guid origamiId, JsonPatchDocument<OrigamiStep> patch)
        {
            await _repository.OrigamiSteps.UpdateOrigamiStep(origamiId, patch);
            await _repository.Save();
        }

        public async Task DeleteStep(Guid origamiId)
        {
            await _repository.OrigamiSteps.DeleteOrigamiStep(origamiId);
            await _repository.Save();
        }
    }
}
