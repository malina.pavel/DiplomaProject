﻿using DIYAppBE.Interfaces;
using DIYAppBE.Models;
using Microsoft.Azure.CognitiveServices.Vision.CustomVision.Prediction;
using Microsoft.Azure.CognitiveServices.Vision.CustomVision.Training;
using System.Text.Json;

namespace DIYAppBE.Services
{
    public class PredictionService : IPredictionService
    {
        private readonly IConfiguration _configuration;
        private readonly string _trainingEndpoint;
        private readonly string _trainingKey;
        private readonly string _predictionEndpoint;
        private readonly string _predictionKey;

        public PredictionService(IConfiguration configuration)
        {
            _configuration = configuration;
            _trainingEndpoint = _configuration["CustomVision:TrainingEndpoint"];
            _trainingKey = _configuration["CustomVision:TrainingKey"];
            _predictionEndpoint = _configuration["CustomVision:PredictionEndpoint"];
            _predictionKey = _configuration["CustomVision:PredictionKey"];
        }

        public async Task<PredictionResult> PredictImageAsync(string imageUrl)
        {
            using var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Prediction-Key", _configuration["CustomVision:PredictionKey"]);

            var body = new { Url = imageUrl };
            var content = new StringContent(JsonSerializer.Serialize(body), System.Text.Encoding.UTF8, "application/json");

            var response = await client.PostAsync(_configuration["CustomVision:Endpoint"], content);
            response.EnsureSuccessStatusCode();

            string responseJson = await response.Content.ReadAsStringAsync();
            var result = JsonSerializer.Deserialize<PredictionResult>(responseJson);
            return result;
        }

        public async Task<CustomVisionTrainingClient> AuthenticateTraining()
        {
            var trainingApi = new CustomVisionTrainingClient
            (new Microsoft.Azure.CognitiveServices.Vision.CustomVision.Training.ApiKeyServiceClientCredentials(_trainingKey))
            {
                Endpoint = _trainingEndpoint
            };

            return trainingApi;
        }

        public async Task<CustomVisionPredictionClient> AuthenticatePrediction()
        {
            var predictionApi = new CustomVisionPredictionClient
            (new Microsoft.Azure.CognitiveServices.Vision.CustomVision.Prediction.ApiKeyServiceClientCredentials(_predictionKey))
            {
                Endpoint = _predictionEndpoint
            };

            return predictionApi;
        }

        public async Task<Microsoft.Azure.CognitiveServices.Vision.CustomVision.Prediction.Models.ImagePrediction> PredictResult(Guid projectId, string imageUrl)
        {
            var trainingApi = await AuthenticateTraining();
            var predictionApi = await AuthenticatePrediction();

            var publishedModelName = (await trainingApi.GetIterationsAsync(projectId)).FirstOrDefault().PublishName;

            if (!string.IsNullOrEmpty(imageUrl))
            {
                var testImage = await ConvertUrlToMemoryStream(imageUrl);
                return predictionApi.ClassifyImage(projectId, publishedModelName, testImage);
            }
            else return null;
        }

        private async Task<Stream> ConvertUrlToMemoryStream(string imageUrl)
        {
            var client = new HttpClient();

            var imageResponse = await client.GetAsync(imageUrl);
            if (imageResponse != null)
            {
                return await imageResponse.Content.ReadAsStreamAsync();
            }
            else return null;
        }
    }
}
