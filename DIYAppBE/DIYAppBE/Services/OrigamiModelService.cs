﻿using AutoMapper;
using DIYAppBE.Interfaces;
using DIYAppBE.Interfaces.Repositories;
using DIYAppBE.Models;
using DIYAppBE.Models.DTOs;
using Microsoft.AspNetCore.JsonPatch;

namespace DIYAppBE.Services
{
    public class OrigamiModelService : IOrigamiModelService
    {
        private readonly IRepositoryManager _repository;
        private readonly IMapper _mapper;

        public OrigamiModelService(IRepositoryManager repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task AddOrigami(OrigamiModel origami)
        {
            await _repository.Origamis.CreateOrigami(origami);
            await _repository.Save();
        }

        public async Task<OrigamiWithStepsDTO> GetOrigami(string slug)
        {
            var origami = await _repository.Origamis.GetOrigami(slug);
            var steps = await _repository.OrigamiSteps.GetStepsFromOrigami(origami!.Id, false);
            var stepsDto = _mapper.Map<List<OrigamiStepDTO>>(steps.ToList());

            var origamiWithStepsDto = _mapper.Map<OrigamiWithStepsDTO>(origami);
            origamiWithStepsDto.Steps = stepsDto;

            return origamiWithStepsDto;
        }

        public async Task<List<OrigamiModel>> GetOrigamis()
        {
            return (await _repository.Origamis.GetOrigamis(false)).ToList();
        }

        public async Task<IList<OrigamiModel>> GetOrigamisByString(string search)
        {
            return (await _repository.Origamis.GetOrigamisByString(search, false)).ToList();
        }

        public async Task UpdateOrigami(Guid origamiId, JsonPatchDocument<OrigamiModel> patch)
        {
            await _repository.Origamis.UpdateOrigami(origamiId, patch);
            await _repository.Save();
        }

        public async Task DeleteOrigami(Guid origamiId)
        {
            await _repository.Origamis.DeleteOrigami(origamiId);
            await _repository.Save();
        }
    }
}
