﻿using AutoMapper;
using DIYAppBE.Interfaces;
using DIYAppBE.Interfaces.Repositories;
using DIYAppBE.Models;
using DIYAppBE.Models.DTOs;
using Microsoft.AspNetCore.JsonPatch;


namespace DIYAppBE.Services
{
    public class PostService : IPostService
    {
        private readonly IRepositoryManager _repository;
        private readonly IMapper _mapper;

        public PostService(IRepositoryManager repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task AddPost(Post post)
        {
            await _repository.Posts.CreatePost(post);
            await _repository.Save();
        }

        public async Task<PostWithMetricsDTO> GetPost(string slug)
        {
            var post = await _repository.Posts.GetPost(slug);
            var comments = await _repository.Comments.GetCommentsByPost(post.Id, false);
            var commentsDto = _mapper.Map<List<CommentDTO>>(comments.ToList());

            var likes = await _repository.Likes.GetLikesByPost(post.Id, false);
            var likesDto = _mapper.Map<List<LikeDTO>>(likes.ToList());

            var postWithMetricsDto = _mapper.Map<PostWithMetricsDTO>(post);
            postWithMetricsDto.Comments = commentsDto;
            postWithMetricsDto.Like = likesDto;

            return postWithMetricsDto;
        }

        public async Task<List<Post>> GetPosts() =>  (await _repository.Posts.GetPosts(false)).ToList();

        public async Task<IList<Post>> GetPostsByString(string search) => (await _repository.Posts.GetPostsByString(search, false)).ToList();

        public async Task UpdatePost(Guid postId, JsonPatchDocument<Post> patch)
        {
            await _repository.Posts.UpdatePost(postId, patch);
            await _repository.Save();
        }

        public async Task DeletePost(Guid postId)
        {
            await _repository.Posts.DeletePost(postId);
            await _repository.Save();
        }
    }
}
