﻿using Microsoft.Azure.CognitiveServices.Vision.CustomVision.Prediction;
using Microsoft.Azure.CognitiveServices.Vision.CustomVision.Training.Models;
using Microsoft.Azure.CognitiveServices.Vision.CustomVision.Training;
using DIYAppBE.Interfaces;

namespace DIYAppBE.Services
{
    public class TrainingService : ITrainingService
    {
        private readonly IConfiguration _configuration;
        private readonly string _trainingEndpoint;
        private readonly string _trainingKey;
        private readonly string _predictionEndpoint;
        private readonly string _predictionKey;
        private readonly string _predictionResource;

        public TrainingService(IConfiguration configuration)
        {
            _configuration = configuration;
            _trainingEndpoint = _configuration["CustomVision:TrainingEndpoint"];
            _trainingKey = _configuration["CustomVision:TrainingKey"];
            _predictionEndpoint = _configuration["CustomVision:PredictionEndpoint"];
            _predictionKey = _configuration["CustomVision:PredictionKey"];
            _predictionResource = _configuration["CustomVision:ResourceId"];
        }

        public async Task<CustomVisionTrainingClient> AuthenticateTraining()
        {
            var trainingApi = new CustomVisionTrainingClient
            (new Microsoft.Azure.CognitiveServices.Vision.CustomVision.Training.ApiKeyServiceClientCredentials(_trainingKey))
            {
                Endpoint = _trainingEndpoint
            };

            return trainingApi;
        }

        public async Task<CustomVisionPredictionClient> AuthenticatePrediction()
        {
            var predictionApi = new CustomVisionPredictionClient
            (new Microsoft.Azure.CognitiveServices.Vision.CustomVision.Prediction.ApiKeyServiceClientCredentials(_predictionKey))
            {
                Endpoint = _predictionEndpoint
            };

            return predictionApi;
        }

        public async Task<Project> CreateProject(string projectName)
        {
            var trainingApi = await AuthenticateTraining();
            return trainingApi.CreateProject(projectName);
        }

        public async Task<Project> GetProjectById(Guid projectId)
        {
            var trainingApi = await AuthenticateTraining();
            return await trainingApi.GetProjectAsync(projectId);
        }

        public async Task<Guid> CreateTag(Guid projectId, string tagName)
        {
            var trainingApi = await AuthenticateTraining();

            var tags = await trainingApi.GetTagsAsync(projectId);
            var tag = tags.FirstOrDefault(t => t.Name == tagName);
            if (tag == null)
            {
                tag = await trainingApi.CreateTagAsync(projectId, tagName);
            }
            return tag.Id;
        }

        public async Task UploadTrainingImages(Guid projectId, List<IFormFile> imageFiles, Guid tagId)
        {
            var trainingApi = await AuthenticateTraining();

            var entries = new List<ImageFileCreateEntry>();
            foreach (var file in imageFiles)
            {
                using (var stream = new MemoryStream())
                {
                    await file.CopyToAsync(stream);
                    entries.Add(new ImageFileCreateEntry(file.FileName, stream.ToArray(), new List<Guid> { tagId }));
                }
            }

            var batch = new ImageFileCreateBatch(entries);
            await trainingApi.CreateImagesFromFilesAsync(projectId, batch);
        }

        public async Task TrainProject(Guid projectId)
        {
            var trainingApi = await AuthenticateTraining();
            var iteration = trainingApi.TrainProject(projectId);

            while (iteration.Status == "Training")
            {
                Thread.Sleep(10000);
                iteration = trainingApi.GetIteration(projectId, iteration.Id);
            }
        }

        public async Task PublishCurrentIteration(Guid projectId)
        {
            var trainingApi = await AuthenticateTraining();
            var project = await GetProjectById(projectId);

            var projectName = project.Name;
            var modelName = projectName.Replace(" ", "") + "Model";
            var iteration = (await trainingApi.GetIterationsAsync(projectId)).LastOrDefault();

            trainingApi.PublishIteration(project.Id, iteration.Id, modelName, _predictionResource);
        }
    }
}
