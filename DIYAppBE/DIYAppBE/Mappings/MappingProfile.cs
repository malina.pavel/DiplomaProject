﻿using DIYAppBE.Models.DTOs;
using DIYAppBE.Models;
using AutoMapper;

namespace DIYAppBE.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Comment, CommentDTO>();
            CreateMap<Like, LikeDTO>();
            CreateMap<Post, PostWithMetricsDTO>();
            CreateMap<ArticleStep, ArticleStepDTO>();
            CreateMap<Article, ArticleWithStepsDTO>();
            CreateMap<OrigamiStep, OrigamiStepDTO>();
            CreateMap<OrigamiModel, OrigamiWithStepsDTO>();
            CreateMap<User, UserSessionDTO>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Username))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.ProfilePicture, opt => opt.MapFrom(src => src.ProfilePicture))
                .ForMember(dest => dest.UserRole, opt => opt.MapFrom(src => src.UserRole));
        }
    }
}
