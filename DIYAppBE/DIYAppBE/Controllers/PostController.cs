﻿using DIYAppBE.Interfaces;
using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;

namespace DIYAppBE.Controllers
{
    [ApiController]
    [Route("posts")]
    public class PostController : ControllerBase
    {
        private readonly IPostService _service;

        public PostController(IPostService service) 
        {
            _service = service;
        }

        [HttpGet]
        public async Task<IActionResult> GetPosts(int pageNumber, int pageSize)
        {
            var posts = await _service.GetPosts();
            var filtered = posts.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return Ok(new { TotalCount = posts.Count(), Posts = filtered } );
        }

        [HttpGet("{slug}")]
        public async Task<IActionResult> GetPost(string slug)
        {
            var post = await _service.GetPost(slug);
            return Ok(post);
        }

        [HttpGet("category/{category}")]
        public async Task<IActionResult> GetPostsByCategory(string category, int pageNumber, int pageSize)
        {
            var posts = (await _service.GetPosts()).FindAll(x => x.Category == category);
            var filtered = posts.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return Ok(new { TotalCount = posts.Count(), Posts = filtered });
        }

        [HttpGet("sort_newest")]
        public async Task<IActionResult> GetNewestPosts(int pageNumber, int pageSize)
        {
            var posts = (await _service.GetPosts()).OrderByDescending(x => x.CreatedDate);
            var filtered = posts.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return Ok(new { TotalCount = posts.Count(), Posts = filtered });
        }

        [HttpGet("sort_popular")]
        public async Task<IActionResult> GetPopularPosts(int pageNumber, int pageSize)
        {
            var posts = await _service.GetPosts();
            var mappedPosts = await MapLikesCount(posts);

            var popularPosts = mappedPosts.OrderByDescending(x => x.Likes);
            var filtered = popularPosts.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return Ok(new { TotalCount = mappedPosts.Count(), Posts = filtered });
        }

        private async Task<List<Post>> MapLikesCount(List<Post> posts)
        {
            var postsWithLikes = new List<Post>();
            foreach (var post in posts)
            {
                var postWithMetrics = await _service.GetPost(post.Slug);
                if (postWithMetrics.Like != null)
                {
                    post.Likes = postWithMetrics.Like.Count();
                }

                var auxPost = post;
                postsWithLikes.Add(auxPost);
            }

            return postsWithLikes;
        }

        [HttpGet("sort_alphabetical")]
        public async Task<IActionResult> GetPostsAlphabetically(int pageNumber, int pageSize)
        {
            var posts = (await _service.GetPosts()).OrderBy(x => x.Title);
            var filtered = posts.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return Ok(new { TotalCount = posts.Count(), Posts = filtered });
        }

        [HttpGet("search")]
        public async Task<IActionResult> GetPostsBySearch([FromQuery] string search, int pageNumber, int pageSize)
        {
            var posts = await _service.GetPostsByString(search);
            var filtered = posts.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return Ok(new { TotalCount = posts.Count(), Posts = filtered });
        }

        [HttpPost("add_post")]
        public async Task<IActionResult> AddPost(Post post)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            await _service.AddPost(post);
            return Ok();
        }

        [HttpPatch("update_post")]
        public async Task<IActionResult> UpdatePost(Guid postId, [FromBody] JsonPatchDocument<Post> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            await _service.UpdatePost(postId, patch);
            return Ok();
        }

        [HttpDelete("delete_post")]
        public async Task<IActionResult> DeletePost(Guid postId)
        {
            await _service.DeletePost(postId);
            return Ok();
        }
    }
}
