﻿using DIYAppBE.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.CognitiveServices.Vision.CustomVision.Training;
using Microsoft.Azure.CognitiveServices.Vision.CustomVision.Training.Models;

namespace DIYAppBE.Controllers
{
    [ApiController]
    [Route("training")]
    public class TrainingController : ControllerBase
    {
        private readonly ITrainingService _trainingService;
        public TrainingController(ITrainingService trainingService)
        {
            _trainingService = trainingService;
        }

        [HttpGet("project_creation")]
        public async Task<IActionResult> ProjectCreation(string projectName)
        {
            Project project = await _trainingService.CreateProject(projectName);
            return Ok(project);
        }

        [HttpGet("get_project_id")]
        public async Task<IActionResult> GetProjectById(Guid projectId)
        {
            Project project = await _trainingService.GetProjectById(projectId);
            return Ok(project);
        }

        [HttpGet("get_project_name")]
        public async Task<IActionResult> GetProjectByName(string name)
        {
            var trainingApi = await _trainingService.AuthenticateTraining();
            var project = trainingApi.GetProjects().FirstOrDefault(p => p.Name == name);
            return Ok(project);
        }

        [HttpPost("tag_training")]
        public async Task<IActionResult> TagTraining(Guid projectId, string tagName, [FromForm] List<IFormFile> imageFiles)
        {
            var tagId = await _trainingService.CreateTag(projectId, tagName);

            await _trainingService.UploadTrainingImages(projectId, imageFiles, tagId);
            return Ok();
        }

        [HttpGet("training_process")]
        public async Task<IActionResult> TrainingProcess(Guid projectId)
        {
            await _trainingService.TrainProject(projectId);
            return Ok();
        }

        [HttpGet("publish_iteration")]
        public async Task<IActionResult> PublishIteration(Guid projectId)
        {
            await _trainingService.PublishCurrentIteration(projectId);
            return Ok();
        }

        [HttpGet("train_and_publish")]
        public async Task<IActionResult> TrainPublishIteration(Guid projectId)
        {
            await _trainingService.TrainProject(projectId);
            await _trainingService.PublishCurrentIteration(projectId);
            return Ok();
        }
    }
}
