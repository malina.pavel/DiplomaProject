﻿using DIYAppBE.Interfaces;
using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace DIYAppBE.Controllers
{
    [ApiController]
    [Route("articles/{articleId}")]
    public class ArticleStepsController : ControllerBase
    {
        private readonly IArticleStepService _service;
        public ArticleStepsController(IArticleStepService service)
        {
            _service = service;
        }

        [HttpGet("steps")]
        public async Task<IActionResult> GetArticleSteps(Guid articleId)
        {
            var steps = (await _service.GetSteps(articleId)).OrderBy(s => s.StepNumber);
            return Ok(steps);
        }

        [HttpPost("steps/add_step")]
        public async Task<IActionResult> AddArticleStep(ArticleStep articleStep)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            await _service.AddStep(articleStep);
            return Ok();
        }

        [HttpPatch("steps/{stepId}/update_step")]
        public async Task<IActionResult> UpdateArticleStep(Guid stepId, [FromBody] JsonPatchDocument<ArticleStep> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            await _service.UpdateStep(stepId, patch);
            return Ok();
        }

        [HttpDelete("steps/{stepId}/delete_step")]
        public async Task<IActionResult> DeleteArticleStep(Guid stepId)
        {
            await _service.DeleteStep(stepId);
            return Ok();
        }
    }
}
