﻿using DIYAppBE.Interfaces;
using DIYAppBE.Models;
using DIYAppBE.Repositories;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace DIYAppBE.Controllers
{
    [ApiController]
    [Route("articles")]
    public class ArticleController : ControllerBase
    {
        private readonly IArticleService _service;

        public ArticleController(IArticleService service) 
        {
            _service = service;
        }

        [HttpGet]
        public async Task<IActionResult> GetArticles()
        {
            var articles = await _service.GetArticles();
            return Ok(articles);
        }

        [HttpGet("published_articles")]
        public async Task<IActionResult> GetPublishedArticles(int pageNumber, int pageSize)
        {
            var articles = await _service.GetPublishedArticles();
            var filtered = articles.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return Ok(new { TotalCount = articles.Count(), Articles = filtered });
        }

        [HttpGet("{slug}")]
        public async Task<IActionResult> GetArticle(string slug)
        {
            var article = await _service.GetArticle(slug);
            article.Steps = article.Steps.OrderBy(a => a.StepNumber).ToList();
            return Ok(article);
        }

        [HttpGet("drafts/{userId}")]
        public async Task<IActionResult> GetDrafts(Guid userId)
        {
            var drafts = await _service.GetDraftsByUser(userId, false);
            return Ok(drafts);
        }


        [HttpGet("category/{category}")]
        public async Task<IActionResult> GetArticlesByCategory(string category, int pageNumber, int pageSize)
        {
            var articles = (await _service.GetPublishedArticles()).FindAll(x => x.Category == category);
            var filtered = articles.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return Ok(new { TotalCount = articles.Count(), Articles = filtered });
        }

        [HttpGet("sort_newest")]
        public async Task<IActionResult> GetNewestArticles(int pageNumber, int pageSize)
        {
            var articles = (await _service.GetPublishedArticles()).OrderByDescending(x => x.PublishedDate);
            var filtered = articles.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return Ok(new { TotalCount = articles.Count(), Articles = filtered });
        }

        [HttpGet("sort_popular")]
        public async Task<IActionResult> GetPopularArticles(int pageNumber, int pageSize)
        {
            var articles = (await _service.GetPublishedArticles()).OrderByDescending(x => x.Views);
            var filtered = articles.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return Ok(new { TotalCount = articles.Count(), Articles = filtered });
        }

        [HttpGet("sort_alphabetical")]
        public async Task<IActionResult> GetArticlesAlphabetically(int pageNumber, int pageSize)
        {
            var articles = (await _service.GetPublishedArticles()).OrderBy(x => x.Title);
            var filtered = articles.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return Ok(new { TotalCount = articles.Count(), Articles = filtered });
        }

        [HttpGet("search")]
        public async Task<IActionResult> GetArticlesBySearch([FromQuery] string search, int pageNumber, int pageSize)
        {
            var articles = (await _service.GetArticlesByString(search));
            var filtered = articles.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return Ok(new { TotalCount = articles.Count(), Articles = filtered });
        }

        [HttpPost("add_article")]
        public async Task<IActionResult> AddArticle(Article article)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            await _service.AddArticle(article);
            return Ok(article);
        }

        [HttpPatch("update_article")]
        public async Task<IActionResult> UpdateArticle(Guid articleId, [FromBody] JsonPatchDocument<Article> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            await _service.UpdateDraftArticle(articleId, patch);
            return Ok();
        }

        [HttpDelete("delete_article")]
        public async Task<IActionResult> DeleteArticle(Guid articleId)
        {
            await _service.DeleteDraftArticle(articleId);
            return Ok();
        }
    }
}
