﻿using DIYAppBE.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Headers;

namespace DIYAppBE.Controllers
{
    [ApiController]
    [Route("upload-image")]
    public class UploadImageController : ControllerBase
    {
        private readonly IUploadImageService _uploadService;
        public UploadImageController(IUploadImageService uploadService)
        {
            _uploadService = uploadService;
        }

        [HttpPost]
        public async Task<IActionResult> Upload(IFormFile imageFile)
        {
            if (imageFile.Length > 0)
            {
                var fileName = ContentDispositionHeaderValue.Parse(imageFile.ContentDisposition).FileName.Trim('"');
                var imageURL = await _uploadService.UploadAsync(imageFile.OpenReadStream(), fileName);
                return Ok(new { imageURL });
            }
            else return BadRequest();
        }
    }
}
