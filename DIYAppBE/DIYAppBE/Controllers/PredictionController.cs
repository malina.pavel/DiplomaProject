﻿using Azure;
using DIYAppBE.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace DIYAppBE.Controllers
{
    [ApiController]
    [Route("prediction")]
    public class PredictionController : ControllerBase
    {
        private readonly IPredictionService _predictionService;
        public PredictionController(IPredictionService predictionService)
        {
            _predictionService = predictionService;
        }

        [HttpGet]
        public async Task<IActionResult> GetPredictionResult(string imageUrl, string requestTag)
        {
            if (!string.IsNullOrEmpty(imageUrl))
            {
                var decodedUrl = System.Web.HttpUtility.UrlDecode(imageUrl);
                var response = await _predictionService.PredictImageAsync(decodedUrl);

                if (response.predictions[0].tagName == requestTag)
                {
                    if (response.predictions[0].probability < 0.7)
                    {
                        return BadRequest(new
                        {
                            Probability = response.predictions[0].probability,
                            Tag = response.predictions[0].tagName,
                            Message = "You're close, but not really... please try again"
                        });
                    }

                    else if (response.predictions[0].probability >= 0.7 && response.predictions[0].probability < 0.85)
                    {
                        return Ok(new
                        {
                            Probability = response.predictions[0].probability,
                            Tag = response.predictions[0].tagName,
                            Message = "This looks ok-ish, you can proceed with the next step... I guess..."
                        });
                    }

                    else return Ok(new
                    {
                        Probability = response.predictions[0].probability,
                        Tag = response.predictions[0].tagName,
                        Message = "Nice one, you can proceed with the next step"
                    });
                }
                else return BadRequest(new {
                    Probability = response.predictions[0].probability,
                    Tag = response.predictions[0].tagName,
                    Message = "This isn't good at all, please try again"
                });
            }
            else return BadRequest(new {
                    Probability = 0,
                    Tag = "none",
                    Message = "Unable to process your image"
            });
        }

        [HttpGet("prediction_result")]
        public async Task<IActionResult> PredictionResult(Guid projectId, string imageUrl, string requestTag)
        {
            var result = await _predictionService.PredictResult(projectId, imageUrl);
            if (result != null)
            {
                if (result.Predictions[0].TagName == requestTag)
                {
                    if (result.Predictions[0].Probability < 0.7)
                    {
                        return BadRequest(new
                        {
                            Probability = result.Predictions[0].Probability,
                            Tag = result.Predictions[0].TagName,
                            Message = "You're close, but not really... please try again"
                        });
                    }

                    else if (result.Predictions[0].Probability >= 0.7 && result.Predictions[0].Probability < 0.85)
                    {
                        return Ok(new
                        {
                            Probability = result.Predictions[0].Probability,
                            Tag = result.Predictions[0].TagName,
                            Message = "This looks ok-ish, you can proceed with the next step... I guess..."
                        });
                    }

                    else return Ok(new
                    {
                        Probability = result.Predictions[0].Probability,
                        Tag = result.Predictions[0].TagName,
                        Message = "Nice one, you can proceed with the next step"
                    });
                }
                else return BadRequest(new
                {
                    Probability = result.Predictions[0].Probability,
                    Tag = result.Predictions[0].TagName,
                    Message = "This isn't good at all, please try again"
                });

            }
            else return BadRequest(new
            {
                Probability = 0,
                Tag = "none",
                Message = "Unable to process your image"
            });
        }
    }
}
