﻿using DIYAppBE.Interfaces;
using DIYAppBE.Models;
using DIYAppBE.Models.DTOs;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace DIYAppBE.Controllers
{
    [ApiController]
    [Route("users")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _service;
        private readonly IUserSigningService _signingService;
        private readonly IEmailConfirmationService _emailService;
        private readonly IJWTService _jwtService;

        public UserController(IUserService service, IUserSigningService userSigningService, IEmailConfirmationService emailService, IJWTService jWTService) 
        {
            _service = service;
            _signingService = userSigningService;
            _emailService = emailService;
            _jwtService = jWTService;
        }

        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            var users = await _service.GetUsers();
            return Ok(users);
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetUser(Guid userId)
        {
            var user = await _service.GetUser(userId);
            return Ok(user);
        }

        #region Registering and activating user account via e-mail

        [HttpPost("add_user")]
        public async Task<IActionResult> AddUser(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            var usersWithTheSameMail = (await _service.GetUsers()).FindAll(a => a.Email == user.Email);
            if (usersWithTheSameMail.Count > 0)
            {
                return BadRequest("This email has already been used");
            }

            var usersWithTheSameUsername = (await _service.GetUsers()).FindAll(a => a.Username == user.Username);
            if (usersWithTheSameUsername.Count > 0)
            {
                return BadRequest("The username must be unique");
            }

            user.EmailToken = _emailService.GenerateEmailToken();

            await _signingService.RegisterUser(user);

            var email = _emailService.ComposeEmail(user, "user-registration");
            var emailConfirm = await _emailService.SendEmailConfirmationAsync(email);
            if(emailConfirm)
            {
                return Ok();
            }
            else return BadRequest("Could not send email confirmation");
        }

        [HttpGet("confirm_email")]
        public async Task<IActionResult> ConfirmEmail(string token, string email)
        {
            if (string.IsNullOrEmpty(token) || string.IsNullOrEmpty(email))
            {
                return BadRequest("Invalid token or email");
            }

            var user = (await _service.GetUsers()).FirstOrDefault(a => a.Email == email);
            if (user == null)
            {
                return NotFound($"User {user.Email} not found");
            }

            if (user.EmailToken != token)
            {
                return BadRequest("Invalid token");
            }

            var patchDocument = new JsonPatchDocument<User>();
            patchDocument.Replace(u => u.EmailConfirmed, true);  // once confirmed, the account is activated
            patchDocument.Replace(u => u.EmailToken, "default");
            await _service.UpdateUser(user.Id, patchDocument);

            return Ok();
        }

        #endregion

        [HttpPost("login")]
        public async Task<IActionResult> Login(UserLoginDTO user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            if (user == null)
            {
                return BadRequest("Invalid client request");
            }

            if (await _signingService.ValidateUser(user))
            {
                var userValidated = await _service.GetUserByString(user.Username);
                if (userValidated.EmailConfirmed == false)
                {
                    return BadRequest("Your account is not activated");
                }
                var token = _jwtService.GenerateToken(userValidated);
                return Ok(new AuthenticationResponse { Token = token });
            }

            return Unauthorized("Invalid credentials, try again");
        }

        // ideally, a partial update on a user should be done
        [HttpPatch("update_user")]
        public async Task<IActionResult> UpdateUser(Guid userId, [FromBody]JsonPatchDocument<User> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            await _service.UpdateUser(userId, patch);
            return Ok();
        }

        #region User password reset

        [HttpGet("password_reset_request")]
        public async Task<IActionResult> SendPasswordResetConfirmation(string email)
        {
            var user = (await _service.GetUsers()).FirstOrDefault(a => a.Email == email);
            if (user == null)
            {
                return NotFound($"User {user.Email} not found");
            }

            var token = _emailService.GenerateEmailToken();
            user.EmailToken = token;
            var patchDocument = new JsonPatchDocument<User>();
            patchDocument.Replace(u => u.EmailToken, token);
            await _service.UpdateUser(user.Id, patchDocument);

            var emailStructure = _emailService.ComposeEmail(user, "forgot-password");
            var emailConfirm = await _emailService.SendEmailConfirmationAsync(emailStructure);
            if (emailConfirm)
            {
                return Ok();
            }
            else return BadRequest("Could not send password reset confirmation");
        }

        [HttpGet("replace_password_confirmation")]
        public async Task<IActionResult> ConfirmPasswordReset(string token, string email)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            if (string.IsNullOrEmpty(token) || string.IsNullOrEmpty(email))
            {
                return BadRequest("Invalid token or email");
            }

            var user = (await _service.GetUsers()).FirstOrDefault(a => a.Email == email);
            if (user == null)
            {
                return NotFound($"User {user.Email} not found");
            }

            if (user.EmailToken != token)
            {
                return BadRequest("Invalid token");
            }
            return Ok();
        }

        [HttpPatch("reset_password")]
        public async Task<IActionResult> ResetPassword(string email, [FromBody] JsonPatchDocument<User> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            var user = (await _service.GetUsers()).FirstOrDefault(a => a.Email == email);
            await _service.UpdateUser(user.Id, patch);

            return Ok();
        }

        #endregion

        [HttpDelete("delete_user")]
        public async Task<IActionResult> DeleteUser(Guid userId)
        {
            await _service.DeleteUser(userId);
            return Ok();
        }
    }
}
