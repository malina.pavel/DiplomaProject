﻿using DIYAppBE.Interfaces;
using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace DIYAppBE.Controllers
{
    [ApiController]
    [Route("posts/{postId}")]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService _service;
        public CommentController(ICommentService service)
        { 
            _service = service;
        }

        [HttpGet("comments/sort_newest")]
        public async Task<IActionResult> GetNewestComments(Guid postId)
        {
            var comments = (await _service.GetComments(postId)).OrderByDescending(c => c.CreatedDate);
            return Ok(comments);
        }

        [HttpPost("comments/add_comment")]
        public async Task<IActionResult> AddComment(Comment comment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            await _service.AddComment(comment);
            return Ok();
        }

        [HttpPatch("comments/{commentId}/update_comment")]
        public async Task<IActionResult> UpdateComment(Guid commentId, [FromBody] JsonPatchDocument<Comment> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            await _service.UpdateComment(commentId, patch);
            return Ok();
        }

        [HttpDelete("comments/{commentId}/delete_comment")]
        public async Task<IActionResult> DeleteComment(Guid commentId)
        {
            await _service.DeleteComment(commentId);
            return Ok();
        }
    }
}
