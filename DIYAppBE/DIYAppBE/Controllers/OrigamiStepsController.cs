﻿using DIYAppBE.Interfaces;
using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace DIYAppBE.Controllers
{
    [ApiController]
    [Route("origamis/{origamiId}")]
    public class OrigamiStepsController : ControllerBase
    {
        private readonly IOrigamiStepService _service;
        public OrigamiStepsController(IOrigamiStepService service)
        {
            _service = service;
        }

        [HttpGet("steps")]
        public async Task<IActionResult> GetOrigamiSteps(Guid origamiId)
        {
            var steps = (await _service.GetSteps(origamiId));
            return Ok(steps);
        }

        [HttpPost("steps/add_step")]
        public async Task<IActionResult> AddOrigamiStep(OrigamiStep origamiStep)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            await _service.AddStep(origamiStep);
            return Ok();
        }

        [HttpPatch("steps/{stepId}/update_step")]
        public async Task<IActionResult> UpdateOrigamiStep(Guid stepId, [FromBody] JsonPatchDocument<OrigamiStep> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            await _service.UpdateStep(stepId, patch);
            return Ok();
        }

        [HttpDelete("steps/{stepId}/delete_step")]
        public async Task<IActionResult> DeleteOrigamiStep(Guid stepId)
        {
            await _service.DeleteStep(stepId);
            return Ok();
        }
    }
}
