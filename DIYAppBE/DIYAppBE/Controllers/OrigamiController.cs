﻿using DIYAppBE.Interfaces;
using DIYAppBE.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace DIYAppBE.Controllers
{
    [ApiController]
    [Route("origamis")]
    public class OrigamiController : ControllerBase
    {
        private readonly IOrigamiModelService _service;

        public OrigamiController(IOrigamiModelService service) 
        {
            _service = service;
        }

        [HttpGet]
        public async Task<IActionResult> GetOrigamis()
        {
            var articles = await _service.GetOrigamis();
            return Ok(articles);
        }

        [HttpGet("{slug}")]
        public async Task<IActionResult> GetOrigami(string slug)
        {
            var article = await _service.GetOrigami(slug);
            return Ok(article);
        }

        [HttpGet("search")]
        public async Task<IActionResult> GetOrigamiBySearch([FromQuery] string search)
        {
            var articles = await _service.GetOrigamisByString(search);
            return Ok(articles);
        }

        [HttpPost("add_origami")]
        public async Task<IActionResult> AddOrigami(OrigamiModel origami)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            await _service.AddOrigami(origami);
            return Ok(origami);
        }

        [HttpPatch("update_origami")]
        public async Task<IActionResult> UpdateOrigami(Guid origamiId, [FromBody] JsonPatchDocument<OrigamiModel> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            await _service.UpdateOrigami(origamiId, patch);
            return Ok();
        }

        [HttpDelete("delete_origami")]
        public async Task<IActionResult> DeleteOrigami(Guid origamiId)
        {
            await _service.DeleteOrigami(origamiId);
            return Ok();
        }
    }
}
