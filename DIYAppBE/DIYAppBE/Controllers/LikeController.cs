﻿using DIYAppBE.Interfaces;
using DIYAppBE.Models;
using Microsoft.AspNetCore.Mvc;

namespace DIYAppBE.Controllers
{
    [ApiController]
    [Route("posts/{postId}")]
    public class LikeController : ControllerBase
    {
        private readonly ILikeService _service;
        public LikeController(ILikeService service)
        {
            _service = service;
        }

        [HttpGet("likes")]
        public async Task<IActionResult> GetLikes(Guid postId)
        {
            var likes = await _service.GetLikes(postId);
            return Ok(likes);
        }

        [HttpPost("likes/increase_likes")]
        public async Task<IActionResult> AddLike(Like like)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            await _service.AddLike(like);
            return Ok();
        }

        [HttpDelete("likes/decrease_likes")]
        public async Task<IActionResult> DeleteLike(Guid userId, Guid postId)
        {
            await _service.DeleteLike(userId, postId);
            return Ok();
        }
    }
}
