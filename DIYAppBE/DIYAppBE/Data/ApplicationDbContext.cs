﻿using DIYAppBE.Models;
using Microsoft.EntityFrameworkCore;

namespace DIYAppBE.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options) { }

        public DbSet<User> RegisteredUsers { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<ArticleStep> ArticleSteps { get; set; }
        public DbSet<Comment> PostComments { get; set; }
        public DbSet<Like> PostLikes { get; set; }
        public DbSet<Post> ForumPosts { get; set; }
        public DbSet<OrigamiModel> OrigamiModels { get; set; }
        public DbSet<OrigamiStep> OrigamiSteps { get; set; }
    }
}
