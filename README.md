# Vulkraft - A Collaborative Platform Dedicated to Do-It-Yourself Projects

#### Link to GitLab repository: https://gitlab.upt.ro/malina.pavel/DiplomaProject
#### Link to Github repository: https://github.com/malinapavel/DiplomaProject

## PREREQUISITES

- frontend: nodejs v18.15.0 installed
- backend: Microsoft Visual Studio with .NET 6 SDK installed
- database: PostgreSQL 16.1 installed
- cloud: Microsoft Azure subscription with the following resources created:
            
            1. Microsoft Azure resource group
            2. Azure Storage account resource
            3. Custom Vision cloud resource (training + prediction)


## PROJECT INSTALLATION

- frontend project: 
    1. install Angular CLI : `npm install -g @angular/cli@16.2.6`
    2. install the following dependencies:

            `npm install @ng-bootstrap/ng-bootstrap@next`

            `npm install @auth0/angular-jwt`

            `npm install --save @ckeditor/ckeditor5-angular`
            
            `npm install ngx-webcam`
- backend project: 
    - since _launchSettings.json_ is hidden from this repository, it is required to create one yourself, in the following format:
      ```json
        {
          "$schema": "https://json.schemastore.org/launchsettings.json",
          "profiles": {
            "DIYAppBE": {
              "commandName": "Project",
              "dotnetRunMessages": true,
              "launchBrowser": true,
              "launchUrl": "swagger",
              "applicationUrl": "https://localhost:7113;http://localhost:5107",
              "environmentVariables": {
                "ASPNETCORE_ENVIRONMENT": "Development"
              }
            }
        }
      ```
    - create an _appsettings.json_ file, since this is also hidden from this repository (see the _OTHER CONFIGURATIONS_ section)
- database:
    - create a new database within the local server, preferrably named 'postgres'
    - in the ASP .NET Core project, do the following:
        
    1. add the connection string to the PostgreSQL database in _appsettings.json_:

            "ConnectionStrings": {
                "local": "Host=localhost; Database=postgres_database_name; Username=your_username; Password=your_password"
            }
    2. perform the database table migrations by running the following commands in Package Manager Console:

            `add-migration Migration_Name`
            
            `update-database`

## RUNNING THE APPLICATION
- frontend project: ng serve and navigate to https://localhost:4200/home
- backend project: press on the green Start arrow from above, indicating the name of the solution (DIYAppBE); the SwaggerUI should open up automatically 

## OTHER CONFIGURATIONS
- frontend project: it is mandatory to generate a self-signed .pem file for SSL certificate, by running: 

        `openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout key.pem -out cert.pem`
- backend project: in _appsettings.json_, the following JSON configuration should be defined:

```javascript
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft.AspNetCore": "Warning"
    }
  },
  "ConnectionStrings": {
    "local": "Host=localhost; Database=postgres_database_name; Username=your_username; Password=your_password",
    "AzureStorage": "DefaultEndpointsProtocol=https;AccountName=diyproject;AccountKey=some_account_key;EndpointSuffix=core.windows.net"
  },
  "AllowedHosts": "*",
  "Jwt": {
    "Issuer": "some_host",
    "Audience": "some_host",
    "Key": "your_custom_key"
  },
  "Hashing": {
    "Pepper": "some_value_as_pepper",
    "Iterations": your_desired_nr_of_iterations
  },
  "Mailjet": {
    "ApiKey": your_api_key,
    "SecretKey": your_secret_key
  },
  "EmailConfirm": {
    "From": "vulkraft@proton.me",
    "ApplicationName": "Vulkraft App",
    "ConfirmationPath": "confirm-email",
    "PasswordRecoveryPath":  "password-reset"
  },
  "CustomVision": {
    "TrainingKey": your_training_key,
    "TrainingEndpoint": your_training_endpoint,
    "PredictionKey": your_prediction_key,
    "PredictionEndpoint": your_prediction_endpoint,
    "ResourceId": your_resource_id
  }
}
```


The keys for the Mailjet API, Azure Storage and Custom Vision resources can be obtained as follows:
1. Azure Storage: 
   - go to the Azure Storage resource, Security + networking section > Access keys and copy one of the two connection string provided;
   - it is indicated to name your Azure Storage as "diyproject",
2. Custom Vision: 
   - go to the settings of the Custom Vision portal and extract the training and prediction keys, endpoints and resource Id of the project resource you're working on
3. Mailjet API: 
   - create an account and provide a sender address, in this case vulkraft@proton.me;
   - go to your account settings > REST API > Api Key Management, where the API key and secret key can be extracted.


