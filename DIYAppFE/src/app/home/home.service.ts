import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Article } from "../shared/models/article";
import { Post } from "../shared/models/post";
import { Origami } from "../shared/models/origami";
import { ArticleResponse } from "../shared/models/articleResponse";
import { PostResponse } from "../shared/models/postResponse";

@Injectable({providedIn: 'root'})

export class HomeService {
    private httpService: HttpClient;

    constructor(httpService: HttpClient) {
        this.httpService = httpService;
    }

    // top 5 most popular articles
    popularArticles(): Observable<ArticleResponse> {
        return this.httpService.get<ArticleResponse>('https://localhost:7113/articles/sort_popular?pageNumber=1&pageSize=5');
    }

    // 5 newest articles
    newestArticles(): Observable<ArticleResponse> {
        return this.httpService.get<ArticleResponse>('https://localhost:7113/articles/sort_newest?pageNumber=1&pageSize=5');
    }

    // top 5 latest posts
    latestPosts(): Observable<PostResponse> {
        return this.httpService.get<PostResponse>('https://localhost:7113/posts/sort_newest?pageNumber=1&pageSize=5');
    }

    // 3 origami modelsto be displayed
    getOrigamis(): Observable<Origami[]> {
        return this.httpService.get<Origami[]>('https://localhost:7113/origamis');
    }
}