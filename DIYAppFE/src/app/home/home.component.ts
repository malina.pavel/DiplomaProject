import { Component, ElementRef, ViewChild } from '@angular/core';
import { HomeService } from './home.service';
import { Article } from '../shared/models/article';
import { Post } from '../shared/models/post';
import { Origami } from '../shared/models/origami';
import { ArticleResponse } from '../shared/models/articleResponse';
import { PostResponse } from '../shared/models/postResponse';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent {
  public popularArticles: Article[] = [];
  public latestPosts: Post[] = [];
  public newestArticles: Article[] = [];
  public origamis: Origami[] = [];

  constructor(private homeService: HomeService) {
    this.loadPopularArticles();
    this.loadLatestPosts();
    this.loadNewestArticles();
    this.loadOrigamis();
  }

  private loadPopularArticles(): void {
    this.homeService.popularArticles().subscribe((data: ArticleResponse) => {
      this.popularArticles = data.articles;
    });
  }

  private loadNewestArticles(): void {
    this.homeService.newestArticles().subscribe((data: ArticleResponse) => {
      this.newestArticles = data.articles;
    });
  }

  private loadLatestPosts(): void {
    this.homeService.latestPosts().subscribe((data: PostResponse) => {
      this.latestPosts = data.posts;
    });
  }

  private loadOrigamis(): void {
    this.homeService.getOrigamis().subscribe((data: Origami[]) => {
      this.origamis = data;
    });
  }
}
