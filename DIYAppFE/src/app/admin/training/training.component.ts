import { Component, OnInit } from '@angular/core';
import { OrigamiWithSteps } from 'src/app/shared/models/origamiWithSteps';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit{
  origami!: OrigamiWithSteps;
  origamiSlug!: string
  private origamiId!: string;
  projectId!: string;

  isLoading: boolean = false;
  isActionAvailable: boolean = true;
  isDone: boolean = false;

  constructor(private adminService: AdminService) { }

  ngOnInit(): void {
    this.loadOrigami();
  }

  private loadOrigami() {
    this.origamiSlug = this.adminService.getOrigamiSlug();
    this.adminService.getOrigamiModel(this.origamiSlug).subscribe((data: OrigamiWithSteps) => {
      this.origami = data;
    })
  }

  trainAndPublish() {
    this.projectId = this.adminService.getProjectId();
    this.isLoading = true;
    this.isActionAvailable = false;
    this.adminService.trainAndPublish(this.projectId).subscribe(() => {
      this.isLoading = false;
      this.isDone = true;
    });
  }
}
