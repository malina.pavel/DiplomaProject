import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { CreateOrigamiComponent } from './create-origami/create-origami.component';
import { AdminService } from './admin.service';
import { ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { CreateOrigamiStepsComponent } from './create-origami-steps/create-origami-steps.component';
import { SharedModule } from '../shared/shared.module';
import { TrainingComponent } from './training/training.component';
import { PredictionTestComponent } from './prediction-test/prediction-test.component';


@NgModule({
  declarations: [
    AdminComponent,
    CreateOrigamiComponent,
    CreateOrigamiStepsComponent,
    TrainingComponent,
    PredictionTestComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ReactiveFormsModule,
    CKEditorModule,
    SharedModule
  ],
  providers: [AdminService]
})
export class AdminModule { }
