import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { ImagePredictionService } from 'src/app/shared/services/image-prediction.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UploadImageService } from 'src/app/shared/services/upload-image.service';
import { OrigamiWithSteps } from 'src/app/shared/models/origamiWithSteps';
import { Router } from '@angular/router';
import { PredictionResponse } from 'src/app/shared/models/predictionResponse';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-prediction-test',
  templateUrl: './prediction-test.component.html',
  styleUrls: ['./prediction-test.component.css']
})
export class PredictionTestComponent implements OnInit{
  imageForm!: FormGroup;
  imageURL!: string;
  private origamiSlug!: string;
  origami!: OrigamiWithSteps;
  private tagName!: string;
  sampleImage!: string;

  predictionResult!: PredictionResponse;
  isStepValid: boolean = false;
  
  constructor(
    private adminService: AdminService, 
    private predictionService: ImagePredictionService,
    private uploadImageService: UploadImageService,
    private formBuilder: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.buildForm();
    this.loadSampleImage();
  }

  private buildForm() {
    this.imageForm = this.formBuilder.group({ image: [null, Validators.required] });
  }
  private loadSampleImage() {
    this.origamiSlug = this.adminService.getOrigamiSlug();
    this.adminService.getOrigamiModel(this.origamiSlug).subscribe((data: OrigamiWithSteps) => {
      this.origami = data;
      if(data.steps) {
        this.sampleImage = data.steps[1].imageURL; // try the second step of the origami game
      }
    });
  }

  uploadImage(e: any) {
    const file = e.target.files[0];
    if (file) {
      this.checkImageSize(file);
    }
  }

  private checkImageSize(image: File) {
      const formData = new FormData();
      formData.append('imageFile', image);

      // if the image is bigger than 10MB, set error
      if(this.uploadImageService.imageSizeExceeded(image)) {
        this.imageForm.get('image')?.setErrors({exceeded: true});
      }
      else { //success
        this.uploadImageService.uploadImage(formData).subscribe((response: any) => {
            console.log(response.imageURL);
            this.imageURL = response.imageURL;
        });
      }
  }

  predictImage() {
    if(this.imageForm.valid) {
      const projectId = this.adminService.getProjectId();
      const requestTag = this.buildTagName();
      this.predictionService.predictImageAlternative(projectId, this.imageURL, requestTag).subscribe({
        next: (data: PredictionResponse) => {
          this.isStepValid = true;
          this.predictionResult = data;
        },
        error: (err: HttpErrorResponse) => {
          this.isStepValid = false;
          this.predictionResult = err.error;
        }
      });
    }
  }

  // try the second step of the origami game
  private buildTagName() {
    const tag = this.origamiSlug?.split("-origami")[0] + '-s2';
    return tag;
  }
}
