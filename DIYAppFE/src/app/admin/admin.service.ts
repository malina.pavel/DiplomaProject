import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { Origami } from '../shared/models/origami';
import { CustomVisionProject } from '../shared/models/customVisionProject';
import { OrigamiWithSteps } from '../shared/models/origamiWithSteps';
import { OrigamiStep } from '../shared/models/origamiStep';

@Injectable({ providedIn: 'root' })
export class AdminService {
  private httpService: HttpClient;

  constructor(httpService: HttpClient) {
    this.httpService = httpService;
  }

  setProjectId(projectId: string) {
    sessionStorage.setItem('customVisionProjectId', projectId);
  }

  setOrigamiSlug(slug: string) {
    sessionStorage.setItem('origamiSlug', slug);
  }

  setOrigamiId(origamiId: string) {
    sessionStorage.setItem('origamiId', origamiId);
  }

  getProjectId(): string {
    return sessionStorage.getItem('customVisionProjectId') || '';
  }

  getOrigamiSlug(): string {
    return sessionStorage.getItem('origamiSlug') || '';
  }

  getOrigamiId(): string {
    return sessionStorage.getItem('origamiId') || '';
  }

  getOrigamiModel(origamiSlug: string): Observable<OrigamiWithSteps> {
    return this.httpService.get<OrigamiWithSteps>('https://localhost:7113/origamis/' + origamiSlug);
  }

  getOrigamiModelSteps(origamiId: string): Observable<OrigamiStep[]> {
    return this.httpService.get<OrigamiStep[]>('https://localhost:7113/origamis/' + origamiId + '/steps');
  }

  addOrigami(origami: Origami): Observable<Origami> {
    return this.httpService.post<Origami>('https://localhost:7113/origamis/add_origami', origami);
  }

  addOrigamiStep(origamiId: string, origamiStep: OrigamiStep): Observable<OrigamiStep> {
    return this.httpService.post<OrigamiStep>('https://localhost:7113/origamis/' + origamiId + '/steps/add_step', origamiStep);
  }

  createTrainingProject(projectName: string): Observable<CustomVisionProject> {
    return this.httpService.get<CustomVisionProject>('https://localhost:7113/training/project_creation?projectName=' + projectName);
  }

  tagTraining(projectId: string, tagName: string, images: FormData) {
    return this.httpService.post('https://localhost:7113/training/tag_training?projectId=' + projectId + 
            '&tagName=' + tagName, images);
  }

  trainingProcess(projectId: string) {
    return this.httpService.get('https://localhost:7113/training/training_process?projectId=' + projectId);
  }

  publishIteration(projectId: string) {
    return this.httpService.get('https://localhost:7113/training/publish_iteration?projectId=' + projectId);
  }

  trainAndPublish(projectId: string) {
    return this.httpService.get('https://localhost:7113/training/train_and_publish?projectId=' + projectId);
  }
}
