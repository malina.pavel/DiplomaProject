import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrigamiComponent } from './create-origami.component';

describe('CreateOrigamiComponent', () => {
  let component: CreateOrigamiComponent;
  let fixture: ComponentFixture<CreateOrigamiComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateOrigamiComponent]
    });
    fixture = TestBed.createComponent(CreateOrigamiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
