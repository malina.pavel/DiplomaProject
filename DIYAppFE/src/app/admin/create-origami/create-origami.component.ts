import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Origami } from 'src/app/shared/models/origami';
import { AdminService } from '../admin.service';
import { Router } from '@angular/router';
import { UploadImageService } from 'src/app/shared/services/upload-image.service';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CustomVisionProject } from 'src/app/shared/models/customVisionProject';

@Component({
  selector: 'app-create-origami',
  templateUrl: './create-origami.component.html',
  styleUrls: ['./create-origami.component.css']
})
export class CreateOrigamiComponent implements OnInit{
  public Editor = ClassicEditor;
  public config = {
    toolbar: ['undo', 'redo', 
              '|', 'heading', 
              '|', 'bold', 'italic',
              '|', 'link', 'insertTable',
              '|', 'bulletedList', 'numberedList'
    ]
  }

  origamiForm!: FormGroup;
  thumbnailURL!: string;

  constructor(
    private formBuilder: FormBuilder, 
    private adminService: AdminService,
    private router: Router,
    private uploadImageService: UploadImageService
  ) { }

  ngOnInit(): void {
    this.buildForm();
  }

  private buildForm() {
    this.origamiForm = this.formBuilder.group(
      {
        name: ['',Validators.required],
        overview: ['', [Validators.required, Validators.minLength(50)]],
        image: [null]
      }
    );
  }

  uploadImage(e: any) {
    const file = e.target.files[0];
    if (file) {
      this.checkImageSize(file);
    }
  }

  private checkImageSize(image: File) {
      const formData = new FormData();
      formData.append('imageFile', image);

      // if the image is bigger than 10MB, set error
      if(this.uploadImageService.imageSizeExceeded(image)) {
        this.origamiForm.get('image')?.setErrors({exceeded: true});
      }
      else { //success
        this.uploadImageService.uploadImage(formData).subscribe((response: any) => {
            console.log(response.imageURL);
            this.thumbnailURL = response.imageURL;
        });
      }
  }

  saveOrigami() {
    if(this.origamiForm.valid) {
      const origamiForm = this.origamiForm.value;
      const origamiName = origamiForm.name;

      const origami: Origami = {
        name: origamiName,
        overview: origamiForm.overview,
        thumbnail: this.thumbnailURL,
        slug: ''
      }

      this.adminService.addOrigami(origami).subscribe((data: Origami) => { 
        this.adminService.setOrigamiSlug(data.slug!);
        this.adminService.setOrigamiId(data.id!);
      });

      this.adminService.createTrainingProject(origamiName).subscribe((data: CustomVisionProject) => {
        this.adminService.setProjectId(data.id);
      });

      this.router.navigate(['origami-game-studio/create-origami-steps']);
    }
  }
}
