import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { OrigamiWithSteps } from 'src/app/shared/models/origamiWithSteps';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UploadImageService } from 'src/app/shared/services/upload-image.service';
import { Router } from '@angular/router';
import { OrigamiStep } from 'src/app/shared/models/origamiStep';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-create-origami-steps',
  templateUrl: './create-origami-steps.component.html',
  styleUrls: ['./create-origami-steps.component.css']
})
export class CreateOrigamiStepsComponent implements OnInit{
  public Editor = ClassicEditor;
  data: any;
  public config = {
    toolbar: ['undo', 'redo', 
              '|', 'heading', 
              '|', 'bold', 'italic',
              '|', 'link', 'insertTable',
              '|', 'bulletedList', 'numberedList'
    ]
  }
  
  origamiSlug!: string
  private origamiId!: string;
  origamiSteps!: OrigamiStep[];
  private tagName!: string;
  projectId!: string;

  private stepSource = new ReplaySubject<OrigamiStep[]>(1);
  stepSource$ = this.stepSource.asObservable();

  origamiStepForm!: FormGroup;
  addedNewStep!: boolean;
  stepNumber: number = 1;

  stepImageURL!: string;
  selectedTrainingImages!: File[];

  constructor(
    private formBuilder: FormBuilder, 
    private adminService: AdminService, 
    private router: Router,
    private uploadImageService: UploadImageService) 
  {}

  ngOnInit(): void {
    this.loadCurrentSteps();
    this.buildForm();
  }

  private buildForm() {
    this.origamiStepForm = this.formBuilder.group(
      {
        description: ['', [Validators.required, Validators.minLength(15)]],
        image: [null, Validators.required],
        training: [null]
      }
    );
  }

  private loadCurrentSteps() {
    this.origamiId = this.adminService.getOrigamiId();
    if(this.origamiId){
      this.adminService.getOrigamiModelSteps(this.origamiId).subscribe((data: OrigamiStep[]) => {
        this.stepSource.next(data);
        this.stepNumber = data.length + 1;
        console.log(this.stepSource$)
      });
    }
  }

  uploadImage(e: any) {
    const file = e.target.files[0];
    if (file) {
      this.checkImageSize(file);
    }
  }

  private checkImageSize(image: File) {
      const formData = new FormData();
      formData.append('imageFile', image);

      // if the image is bigger than 10MB, set error
      if(this.uploadImageService.imageSizeExceeded(image)) {
        this.origamiStepForm.get('image')?.setErrors({exceeded: true});
      }
      else { //success
        this.uploadImageService.uploadImage(formData).subscribe((response: any) => {
            console.log(response.imageURL);
            this.stepImageURL = response.imageURL;
        });
      }
  }

  uploadTrainingImages(e: any) {
    this.selectedTrainingImages = e.target.files;
  }

  addOrigamiStep() {
    this.projectId = this.adminService.getProjectId();
    this.origamiSlug = this.adminService.getOrigamiSlug();
    this.origamiId = this.adminService.getOrigamiId();
    this.tagName = this.buildTagName();

    if(this.origamiStepForm.valid && this.selectedTrainingImages.length > 0) {
      const origamiStepForm = this.origamiStepForm.value;

      const step: OrigamiStep = {
        origamiId: this.origamiId,
        stepNumber: this.stepNumber,
        description: origamiStepForm.description,
        imageURL: this.stepImageURL
      }

      const formData = new FormData();
      for(let file of this.selectedTrainingImages) {
        formData.append('imageFiles', file, file.name);
      }

      this.adminService.addOrigamiStep(this.origamiId, step).subscribe((data: OrigamiStep) => { 
        this.loadCurrentSteps();
      });
      this.adminService.tagTraining(this.projectId, this.tagName, formData).subscribe(() => { })

      //this.stepNumber++;
      this.addedNewStep = false;

      this.stepImageURL = '';
      this.selectedTrainingImages = [];
      this.origamiStepForm.reset();
    }
  }

  private buildTagName() {
    const tag = this.origamiSlug?.split("-origami")[0] + '-s' + this.stepNumber;
    return tag;
  }
}
