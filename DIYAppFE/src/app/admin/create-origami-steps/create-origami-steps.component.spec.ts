import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrigamiStepsComponent } from './create-origami-steps.component';

describe('CreateOrigamiStepsComponent', () => {
  let component: CreateOrigamiStepsComponent;
  let fixture: ComponentFixture<CreateOrigamiStepsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateOrigamiStepsComponent]
    });
    fixture = TestBed.createComponent(CreateOrigamiStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
