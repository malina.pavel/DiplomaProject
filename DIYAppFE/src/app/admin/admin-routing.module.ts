import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { CreateOrigamiComponent } from './create-origami/create-origami.component';
import { CreateOrigamiStepsComponent } from './create-origami-steps/create-origami-steps.component';
import { TrainingComponent } from './training/training.component';
import { PredictionTestComponent } from './prediction-test/prediction-test.component';

const routes: Routes = [
  { path: '', component: AdminComponent },
  { path: 'create-origami', component: CreateOrigamiComponent },
  { path: 'create-origami-steps', component: CreateOrigamiStepsComponent },
  { path: 'training', component: TrainingComponent },
  { path: 'prediction-test', component: PredictionTestComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
