import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticlesComponent } from './articles.component';
import { ViewArticleComponent } from './view-article/view-article.component';
import { ArticleEditorComponent } from './article-editor/article-editor.component';
import { AuthGuard } from '../shared/guard/auth.guard';

const routes: Routes = [
  { path: '', component: ArticlesComponent },
  { path: 'editor', component: ArticleEditorComponent, canActivate: [AuthGuard] },
  { path: 'editor/:slug', component: ArticleEditorComponent, canActivate: [AuthGuard] },
  { path: ':slug', component: ViewArticleComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ArticlesRoutingModule {}
