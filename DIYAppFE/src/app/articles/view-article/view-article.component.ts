import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArticleService } from '../articles.service';
import { Article } from 'src/app/shared/models/article';
import { ArticleWithSteps } from 'src/app/shared/models/articleWithSteps';
import { UserSession } from 'src/app/shared/models/userSession';
import { ArticleResponse } from 'src/app/shared/models/articleResponse';
import { Subscription } from 'rxjs';
import { UserRegisteredService } from 'src/app/shared/services/user-registered.service';

@Component({
  selector: 'app-view-article',
  templateUrl: './view-article.component.html',
  styleUrls: ['./view-article.component.css']
})
export class ViewArticleComponent implements OnInit, OnDestroy{
  articleSlug!: string;
  article!: ArticleWithSteps;
  similarArticles!: Article[];
  userNameMap: { [userId: string]: string } = {};
  routeSubscription!: Subscription;

  constructor(private route: ActivatedRoute, 
    private articleService: ArticleService,
    private userRegisteredService: UserRegisteredService
  ) { }

  ngOnInit(): void {
    this.routeSubscription = this.route.params.subscribe(params => {
      this.articleSlug = params['slug'];
      this.loadArticle(this.articleSlug);
    })
  }

  ngOnDestroy(): void {
    this.routeSubscription.unsubscribe();
  }

  private loadArticle(slug: string): void {
    this.articleService.getArticle(slug).subscribe((data: ArticleWithSteps) => {
      this.article = data;
      this.getUser(data);
      this.getArticlesFromCategory(data.category);
    })
  }

  private getUser(article: Article) {
    this.userRegisteredService.getUser(article.userId).subscribe((data: UserSession) => {
        this.userNameMap[article.userId] = data.firstName + ' ' + data.lastName;
    });
  }

  private getArticlesFromCategory(category: string) {
    this.articleService.getArticlesByCategory(1, 6, category).subscribe((data: ArticleResponse) => {
        this.similarArticles = data.articles;
        this.similarArticles = this.similarArticles.filter(a => a.slug != this.article.slug)
    })
  }

  copyToClipboard() {
    navigator.clipboard.writeText('https://localhost:4200/articles/' + this.article.slug).then(() => {
      const popover = document.getElementById('myPopover');
      if(popover) popover.style.display = 'block'; 

      setTimeout(() => {
        if(popover) popover.style.display = 'none';
      }, 2000);
    });
  }
}
