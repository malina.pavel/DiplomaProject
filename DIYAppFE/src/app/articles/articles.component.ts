import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Article } from '../shared/models/article';
import { ArticleService } from './articles.service';
import { ArticleResponse } from '../shared/models/articleResponse';
import { UserSession } from '../shared/models/userSession';
import { DropdownCategories } from '../shared/enums/dropdown-categories';
import { UserRegisteredService } from '../shared/services/user-registered.service';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit{
  articles!: Article[];
  userNameMap: { [userId: string]: string } = {};
  dropdownCategories = Object.values(DropdownCategories);
  isUserLoggedIn!: boolean;

  currentPage: number = 1;
  totalPosts:number = 0;
  totalPages: number = 0;
  totalPagesArray: number[] = [];
  pageSize: number = 6; // hardcoded for a better view on the UI

  actionType?: string;
  @ViewChild('searchInput') searchInputRef!: ElementRef;
  keyword!: string;
  categoryOption!: string

  constructor(private articleService: ArticleService, private userRegisteredService: UserRegisteredService) {
    this.loadArticles(this.currentPage); // initially loading all the articles as they are
  }

  ngOnInit(): void {
    const userIdLoggedIn = this.userRegisteredService.getUserId();
    this.isUserLoggedIn = userIdLoggedIn != '' ? true : false;
  }

  loadArticles(page: number): void{
    switch(this.actionType){
      case 'popular': this.sortByPopularity(page); break;
      case 'latest':  this.sortByNewest(page); break;
      case 'alphabetically': this.sortAlphabetically(page); break;
      case 'category': this.getArticleByCategory(page); break;
      case 'search': this.searchArticle(page); break;
      default: this.getAllArticles(page);
    } 
  }  
  
  updateActionType(value: string) {
    this.actionType = value;
    if(this.searchInputRef.nativeElement.value) {
      this.keyword = this.searchInputRef.nativeElement.value;
      this.searchInputRef.nativeElement.value='';
    }

    this.loadArticles(1); // start to display data from the first page
  }

  updateCategories(value: string, category: string) {
    this.actionType = value;
    this.categoryOption = category;
    this.loadArticles(1); // start to display data from the first page
  }

  private getAllArticles(page: number): void {
    this.articleService.getArticles(page, this.pageSize).subscribe((data: ArticleResponse) => {
      this.initialize(page, data);
    });
  }

  private sortByPopularity(page: number): void {
    this.articleService.getPopularArticles(page, this.pageSize).subscribe((data: ArticleResponse) => {
      this.initialize(page, data);
    });
  }

  private sortByNewest(page: number): void {
    this.articleService.getNewestArticles(page, this.pageSize).subscribe((data: ArticleResponse) => {
      this.initialize(page, data);
    });
  }

  private sortAlphabetically(page: number): void {
    this.articleService.getArticlesAlphabetically(page, this.pageSize).subscribe((data: ArticleResponse) => {
      this.initialize(page, data);
    });
  }

  private getArticleByCategory(page: number): void {
    this.articleService.getArticlesByCategory(page, this.pageSize, this.categoryOption).subscribe((data: ArticleResponse) => {
      this.initialize(page, data);
    });
  }

  private searchArticle(page: number): void {
    this.articleService.searchArticles(page, this.pageSize, this.keyword).subscribe((data: ArticleResponse) => {
      this.initialize(page, data);
    });
  }

  
  private initialize(page: number, response: ArticleResponse) {
    this.setArticles(response);
    this.computePageNumber(page, response.totalCount);
    window.scrollTo(0,0);
  }

  private setArticles(response: ArticleResponse): void {
    this.articles = response.articles;
    this.getUser(this.articles);
  }

  private computePageNumber(page: number, totalCount: number) {
    this.totalPages = Math.ceil(totalCount / this.pageSize);
    this.totalPagesArray = new Array(this.totalPages);
    this.currentPage = page;
  }

  private getUser(articles: Article[]) {
    articles.forEach((article: Article) => {
      this.userRegisteredService.getUser(article.userId).subscribe((data: UserSession) => {
        this.userNameMap[article.userId] = data.firstName + ' ' + data.lastName;
      });
    })
  }
}