import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticlesRoutingModule } from './articles-routing.module';
import { ArticlesComponent } from './articles.component';
import { ViewArticleComponent } from './view-article/view-article.component';
import { ArticleEditorComponent } from './article-editor/article-editor.component';
import { ArticleService } from './articles.service';
import { SharedModule } from '../shared/shared.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ReactiveFormsModule } from '@angular/forms';
import { UserRegisteredService } from '../shared/services/user-registered.service';
import { UploadImageService } from '../shared/services/upload-image.service';


@NgModule({
  declarations: [
    ArticlesComponent,
    ViewArticleComponent,
    ArticleEditorComponent
  ],
  imports: [
    CommonModule,
    ArticlesRoutingModule,
    SharedModule,
    CKEditorModule,
    ReactiveFormsModule
  ],
  providers: [ArticleService, UserRegisteredService, UploadImageService]
})
export class ArticlesModule { }
