import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ArticleService } from '../articles.service';
import { UserSession } from 'src/app/shared/models/userSession';
import { Article } from 'src/app/shared/models/article';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { DropdownCategories } from 'src/app/shared/enums/dropdown-categories';
import { Subscription } from 'rxjs';
import { ArticleWithSteps } from 'src/app/shared/models/articleWithSteps';
import { TEMP_IP } from 'src/app/shared/constants/tempIP';
import { ArticleStep } from 'src/app/shared/models/articleStep';
import { UserRegisteredService } from 'src/app/shared/services/user-registered.service';
import { UploadImageService } from 'src/app/shared/services/upload-image.service';

@Component({
  selector: 'app-article-editor',
  templateUrl: './article-editor.component.html',
  styleUrls: ['./article-editor.component.css']
})
export class ArticleEditorComponent implements OnInit{
  public Editor = ClassicEditor;
  data: any;
  public config = {
    toolbar: ['undo', 'redo', 
              '|', 'heading', 
              '|', 'bold', 'italic',
              '|', 'link', 'insertTable',
              '|', 'bulletedList', 'numberedList'
    ]
  }

  articleForm!: FormGroup;
  articleStepForm!: FormGroup;
  dropdownCategories = Object.values(DropdownCategories);
  addedNewStep: boolean = false;
  stepNumber: number = 1;

  private userIdLoggedIn!: string;
  user!: UserSession;
  articleSlug!: string;
  article!: ArticleWithSteps;
  articleId!: string;
  articleSteps!: ArticleStep[];
  tempSteps: ArticleStep[] = [];
  patchBuilder: any = [];
  previousArticleValues: any = {};
  previousStepValues: any = {};

  thumbnailImageURL!: string;
  stepImageURL!: string;

  routeSubscription!: Subscription;

  constructor(
    private formBuilder: FormBuilder, 
    private articleService: ArticleService,
    private router: Router,
    private route: ActivatedRoute,
    private userRegisteredService: UserRegisteredService,
    private uploadImageService: UploadImageService
  ) { }

  ngOnInit(): void {
    this.userIdLoggedIn = this.userRegisteredService.getUserId();
    this.getUser();

    this.routeSubscription = this.route.params.subscribe(params => {
      this.articleSlug = params['slug'];
    });

    // if a draft is opened
    if(this.articleSlug) {
      this.loadArticle(this.articleSlug);
    }
    this.buildInitialForms();
  }

  private getUser() {
    this.userRegisteredService.getUser(this.userIdLoggedIn).subscribe((data: UserSession) => {
      this.user = data;
    });
  }

  private loadArticle(slug: string): void {
    this.articleService.getArticle(slug).subscribe((data: ArticleWithSteps) => {
      this.article = data;
      if(data.steps) this.articleSteps = data.steps;
      this.stepNumber = this.articleSteps.length + 1;
      this.buildPopulatedArticleForm(); // populate fields with already existing data to be updated
    })
  }

  private buildInitialForms() {
    this.articleForm = this.formBuilder.group(
      {
        title: ['',[Validators.required, Validators.minLength(10)]],
        overview: ['', [Validators.required, Validators.minLength(50)]],
        image: ['', Validators.required],
        category: ['', Validators.required]
      }
    );

    this.articleStepForm = this.formBuilder.group(
      {
        title: ['',[Validators.required, Validators.maxLength(20)]],
        overview: ['', [Validators.required, Validators.minLength(50)]],
        image: [null]
      }
    );
  }

  private buildPopulatedArticleForm() {
    const articleTitle = this.article ? this.article.title : '';
    const articleOverview = this.article ? this.article.overview : '';
    const articleCategory = this.article ? this.article.category : '';

    this.articleForm = this.formBuilder.group(
      {
        title: [articleTitle,[Validators.required, Validators.minLength(10)]],
        overview: [articleOverview, [Validators.required, Validators.minLength(50)]],
        image: [null],
        category: [articleCategory, Validators.required]
      }
    );

    // check for changes
    this.patchBuilder = [];
    this.previousArticleValues = { ...this.articleForm.value };
    this.articleForm.valueChanges.subscribe((value) => {
      this.checkArticleChanges();
    });
  }

  // this is for updating an already existing step
  private buildPopulatedStepForm(step: ArticleStep) {
    console.log(step.stepNumber)
    const stepTitle = this.articleSlug && step ? step.title : '';
    const stepOverview = this.articleSlug && step ? step.overview : '';

    this.articleStepForm = this.formBuilder.group(
      {
        title: [stepTitle,[Validators.required, Validators.maxLength(20)]],
        overview: [stepOverview, [Validators.required, Validators.minLength(50)]],
        image: [null]
      }
    );
  }

  editArticle() {
    var userId = '';

    if(this.articleForm.valid) {
      const articleForm = this.articleForm.value;

      if(this.user.id) {
        userId = this.user.id;
      }

      const article: Article = {
        userId: userId,
        title: articleForm.title,
        overview: articleForm.overview,
        thumbnail: this.thumbnailImageURL,
        category: articleForm.category,
        isPublished: false,
        slug: ''
      }

      // first time editing
      if(!this.article) {
        this.addArticle(article);
        this.router.navigate(['/articles']);
      }
      else {
        // handle article update
        if(this.article.id) {
            this.updateArticle(this.article.id);
            this.addArticleSteps(this.article);
        }
      }
    }
  }

  private addArticle(article: Article) {
    this.articleService.addArticle(article).subscribe({
      next: (data: Article) => {
        console.log("success")
        this.addArticleSteps(data);
      },
      error: (err: HttpErrorResponse) => console.log(err)
    });
  }

  private updateArticle(articleId: string) {
    if(this.thumbnailImageURL) {
      this.patchBuilder.push({
        op: 'replace',
        path: '/thumbnail',
        value: this.thumbnailImageURL,
      });
    }

      this.articleService.updateArticle(articleId, this.patchBuilder).subscribe((data: Article) => {
        this.router.navigate(['/drafts']);
      });
  }

  private addArticleSteps(article: Article) {
    this.tempSteps.forEach((step: ArticleStep) => {
      if(article.id){
        step.articleId = article.id;
        this.articleService.addArticleStep(article.id, step).subscribe((step: ArticleStep) => { })
      }
    });
  }

  private checkArticleChanges(): void {
    var currValues = this.articleForm.value;

    Object.keys(currValues).forEach((key) => {
      if (currValues[key] != this.previousArticleValues[key] && key != 'image') { // the image will be handled separately
        this.previousArticleValues[key] = currValues[key];
        this.patchBuilder = this.patchBuilder.filter(
          (patch: { path: string }) => patch.path !== `/${key}`
        );
        this.patchBuilder.push({
          op: 'replace',
          path: `/${key}`,
          value: currValues[key],
        });
      }
    });
  }



  editArticleStep() {
    if(this.articleStepForm.valid) {
      const articleStepForm = this.articleStepForm.value;

      if(!this.article) this.articleId = TEMP_IP;
      else this.articleId = this.article.id!;

      const articleStep: ArticleStep = {
        articleId: this.articleId,
        title: articleStepForm.title,
        overview: articleStepForm.overview,
        imageURL: this.stepImageURL,
        stepNumber: this.stepNumber
      }

      this.stepNumber++;
      this.addedNewStep = false;

      this.tempSteps.push(articleStep);

      this.stepImageURL = '';
      this.articleStepForm.reset();
    }
  }



  uploadImage(e: any, form: FormGroup, urlType: string) {
    const file = e.target.files[0];
    if (file) {
      this.checkImageSize(file, form, urlType);
    }
  }

  private checkImageSize(image: File, form: FormGroup, urlType: string) {
      const formData = new FormData();
      formData.append('imageFile', image);

      // if the image is bigger than 10MB, set error
      if(this.uploadImageService.imageSizeExceeded(image)) {
        form.get('image')?.setErrors({exceeded: true});
      }
      else { //success
        this.uploadImageService.uploadImage(formData).subscribe((response: any) => {
            console.log(response.imageURL);
            this.setURL(urlType, response.imageURL);
        });
      }
  }

  private setURL(type: string, imageURL: string) {
    switch(type) {
      case 'thumbnailURL': 
            this.thumbnailImageURL = imageURL;
            break;

      case 'stepURL': 
            this.stepImageURL = imageURL; 
            break;
      default: console.log("invalid")
    }
  }

  cancelForm(): void {
    const tempStepsLength = this.tempSteps.length;
    for(var i=0; i < tempStepsLength; i++) {
      this.tempSteps.pop()
    }
    
    this.articleForm.reset();
    this.articleStepForm.reset();
    this.router.navigate(['/articles'])
  }

  onChange({editor}: any) {
    this.data = editor.getData();
  }

}
