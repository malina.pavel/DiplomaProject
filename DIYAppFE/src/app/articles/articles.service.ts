import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ArticleResponse } from '../shared/models/articleResponse';
import { UserSession } from '../shared/models/userSession';
import { Article } from '../shared/models/article';
import { ArticleWithSteps } from '../shared/models/articleWithSteps';
import { ArticleStep } from '../shared/models/articleStep';

@Injectable({ providedIn: 'root' })
export class ArticleService {
  private httpService: HttpClient;

  constructor(httpService: HttpClient) {
    this.httpService = httpService;
  }

  getArticles(pageNumber: number, pageSize: number): Observable<ArticleResponse> {
    return this.httpService.get<ArticleResponse>('https://localhost:7113/articles/published_articles?pageNumber=' + pageNumber + '&pageSize=' + pageSize);
  }

  getArticle(slug: string): Observable<ArticleWithSteps> {
    return this.httpService.get<ArticleWithSteps>('https://localhost:7113/articles/' + slug);
  }

  getPopularArticles(pageNumber: number, pageSize: number): Observable<ArticleResponse> {
    return this.httpService.get<ArticleResponse>('https://localhost:7113/articles/sort_popular?pageNumber=' + pageNumber + '&pageSize=' + pageSize);
  }

  getNewestArticles(pageNumber: number, pageSize: number): Observable<ArticleResponse> {
    return this.httpService.get<ArticleResponse>('https://localhost:7113/articles/sort_newest?pageNumber=' + pageNumber + '&pageSize=' + pageSize);
  }

  getArticlesAlphabetically(pageNumber: number, pageSize: number): Observable<ArticleResponse> {
    return this.httpService.get<ArticleResponse>('https://localhost:7113/articles/sort_alphabetical?pageNumber=' + pageNumber + '&pageSize=' + pageSize);
  }

  getArticlesByCategory(pageNumber: number, pageSize: number, category: string): Observable<ArticleResponse> {
    return this.httpService.get<ArticleResponse>('https://localhost:7113/articles/category/' + category + 
            '?pageNumber=' + pageNumber + '&pageSize=' + pageSize);
  }

  searchArticles(pageNumber: number, pageSize: number, keyword: string): Observable<ArticleResponse> {
    return this.httpService.get<ArticleResponse>('https://localhost:7113/articles/search?search=' + keyword + 
            '&pageNumber=' + pageNumber + '&pageSize=' + pageSize);
  }

  addArticle(article: Article): Observable<Article> {
    return this.httpService.post<Article>('https://localhost:7113/articles/add_article', article);
  }

  updateArticle(articleId: string, patch: any[]): Observable<Article> {
    return this.httpService.patch<Article>('https://localhost:7113/articles/update_article?articleId=' + articleId, patch);
  }

  getArticleSteps(articleId: string): Observable<ArticleStep[]> {
    return this.httpService.get<ArticleStep[]>('https://localhost:7113/articles/' + articleId + '/steps');
  }

  addArticleStep(articleId: string, articleStep: ArticleStep): Observable<ArticleStep> {
    return this.httpService.post<ArticleStep>('https://localhost:7113/articles/' + articleId + '/steps/add_step', articleStep);
  }

  updateArticleStep(articleId: string, stepId: string, patch: any[]): Observable<ArticleStep> {
    return this.httpService.patch<ArticleStep>('https://localhost:7113/articles/' + articleId + '/steps/' + stepId + '/update_step', patch);
  }

  deleteArticleStep(articleId: string, stepId: string): Observable<ArticleStep> {
    return this.httpService.delete<ArticleStep>('https://localhost:7113/articles/' + articleId + '/steps/' + stepId + '/delete_step');
  }
}
