import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoreComponent } from './core/core.component';
import { AuthGuard } from './shared/guard/auth.guard';

const routes: Routes = [
  {
    // this path will contain redirections to other modules within CoreComponent (contains header & footer)
    // since there is needed to have the header and footer on each of the pages
    path: '',
    component: CoreComponent,
    children: [
      { path: '', redirectTo: '/home', pathMatch: 'full' },
      {
        path: 'home',
        loadChildren: () => import('./home/home.module').then((m) => m.HomeModule)
      },
      {
        path: 'about-us',
        loadChildren: () => import('./about-us/about-us.module').then((m) => m.AboutUsModule),
      },
      {
        path: 'drafts',
        loadChildren: () => import('./draft/draft.module').then((m) => m.DraftModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'articles',
        loadChildren: () => import('./articles/articles.module').then((m) => m.ArticlesModule),
      },
      {
        path: 'forum',
        loadChildren: () => import('./forum/forum.module').then((m) => m.ForumModule),
      },
      {
        path: 'profile',
        loadChildren: () => import('./user-profile/user-profile.module').then((m) => m.UserProfileModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'origamis',
        loadChildren: () => import('./origamis/origamis.module').then((m) => m.OrigamisModule),
      },
      { path: 'origami-game-studio', 
        loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
        canActivate: [AuthGuard]
      }
    ]
  },
  // the following pages will not have the header and footer
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then((m) => m.LoginModule),
  },
  {
    path: 'registration',
    loadChildren: () =>
      import('./registration/registration.module').then((m) => m.RegistrationModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { anchorScrolling: 'enabled' })],
  exports: [RouterModule],
  providers: [],
})
export class AppRoutingModule {}
