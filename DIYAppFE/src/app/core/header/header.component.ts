import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Subscription, filter } from 'rxjs';
import { UserSession } from 'src/app/shared/models/userSession';
import { UserUpdateService } from 'src/app/shared/services/user-update.service';
import { UserRegisteredService } from 'src/app/shared/services/user-registered.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit, OnDestroy{
  user?: UserSession;
  subscription: Subscription = new Subscription();
  private userIdLoggedIn!: string;
  isUserLoggedIn!: boolean;
  userRole!: string;
  
  constructor(
    private router: Router,
    private userUpdateService: UserUpdateService,
    private userRegisteredService: UserRegisteredService
  ) { }

  ngOnInit() {
    this.userIdLoggedIn = this.userRegisteredService.getUserId();
    this.isUserLoggedIn = this.userIdLoggedIn != '' ? true : false;
    this.userRole = this.userRegisteredService.getUserRole();
    this.getLoggedInUser();

    // forcing the header to refresh after saving the profile changes
    // saving the changes will redirect to the profile page
    // and this ensures the successful navigation triggers the event
    this.subscription.add(
      this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(() => {
        if(this.userIdLoggedIn) {
          this.refreshUserData();
        }
      })
    );

    // intercept new user data
    this.subscription.add(
      this.userUpdateService.userUpdateSource$.subscribe(user => {
        if(user) this.user = user;
      })
    );
  }

  ngOnDestroy() {
      this.subscription.unsubscribe();
  }

  private getLoggedInUser(): void {
    this.userRegisteredService.getUser(this.userIdLoggedIn).subscribe((data: UserSession) => { 
      this.user = data;
    });
  }

  private refreshUserData(): void {
    this.userRegisteredService.getUser(this.userIdLoggedIn).subscribe((data: UserSession) => {
        this.userUpdateService.updateUser(data)
    });
  }

  signOut() {
    localStorage.clear();
    this.userIdLoggedIn = '';
    this.isUserLoggedIn = false;
    this.router.navigate(['/home']);
  }
}
