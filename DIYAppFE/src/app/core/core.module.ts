import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { CoreComponent } from './core.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { JwtModule } from '@auth0/angular-jwt';
import { FormsModule } from '@angular/forms';
import { UserUpdateService } from '../shared/services/user-update.service';
import { SharedModule } from '../shared/shared.module';
import { UserRegisteredService } from '../shared/services/user-registered.service';

export function tokenGetter() {
  return localStorage.getItem('jwt');
}

@NgModule({
  declarations: [CoreComponent, HeaderComponent, FooterComponent],
  imports: [
    CommonModule,
    CoreRoutingModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ['localhost:7113'],
        disallowedRoutes: [],
      },
    }),
    FormsModule,
    SharedModule
  ],
  exports: [HeaderComponent, FooterComponent],
  providers: [UserUpdateService, UserRegisteredService],
})
export class CoreModule {}
