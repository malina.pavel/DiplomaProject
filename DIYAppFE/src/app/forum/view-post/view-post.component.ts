import { Component, OnInit } from '@angular/core';
import { ForumService } from '../forum.service';
import { UserRegisteredService } from 'src/app/shared/services/user-registered.service';
import { PostWithMetrics } from 'src/app/shared/models/postWithMetrics';
import { Post } from 'src/app/shared/models/post';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { UserSession } from 'src/app/shared/models/userSession';
import { PostResponse } from 'src/app/shared/models/postResponse';
import { Like } from 'src/app/shared/models/like';

@Component({
  selector: 'app-view-post',
  templateUrl: './view-post.component.html',
  styleUrls: ['./view-post.component.css'],
})
export class ViewPostComponent implements OnInit {
  postSlug!: string;
  private postId!: string;
  private userIdLoggedIn!: string;
  isUserLoggedIn!: boolean;
  post!: PostWithMetrics;
  similarPosts!: Post[];
  usernameMap: { [userId: string]: string } = {};
  isLiked!: boolean;
  routeSubscription!: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private forumService: ForumService,
    private userRegisteredService: UserRegisteredService
  ) {}

  ngOnInit(): void {
    this.userIdLoggedIn = this.userRegisteredService.getUserId();
    this.isUserLoggedIn = this.userIdLoggedIn != '' ? true : false;
    this.routeSubscription = this.route.params.subscribe((params) => {
      this.postSlug = params['slug'];
      this.loadPost(this.postSlug);
    });
  }

  private loadPost(slug: string): void {
    this.forumService.getPost(slug).subscribe((data: PostWithMetrics) => {
      this.post = data;
      this.postId = data.id;
      this.getUser(data.userId);
      this.getPostsFromCategory(data.category);
      this.loadUserLike(this.postId);
    });
  }

  private getUser(userId: string) {
    this.userRegisteredService.getUser(userId).subscribe((data: UserSession) => {
        this.usernameMap[userId] = data.username;
    });
  }

  private loadUserLike(postId: string) {
    this.forumService.getLikes(postId).subscribe((data: Like[]) => {
        const totalPostLikes = data;
        totalPostLikes.forEach((like: Like) => {
          if (like.userId == this.userIdLoggedIn) this.isLiked = true;
        });
    });
  }

  private getPostsFromCategory(category: string) {
    this.forumService.getPostsByCategory(1, 6, category).subscribe((data: PostResponse) => {
        this.similarPosts = data.posts;
        this.similarPosts = this.similarPosts.filter((a) => a.slug != this.post.slug);
    });
  }

  likePost() {
    if (this.isUserLoggedIn) {
      const like: Like = {
        userId: this.userIdLoggedIn,
        postId: this.postId
      };

      this.forumService.increaseLikes(this.postId, like).subscribe((data: Like) => {
        this.isLiked = true;
        this.loadPost(this.postSlug);
      });
    }
  }

  dislikePost() {
    if (this.isUserLoggedIn) {
      this.forumService.decreaseLikes(this.postId, this.userIdLoggedIn).subscribe((data: Like) => {
        this.isLiked = false;
        this.loadPost(this.postSlug);
      });
    }
  }
}
