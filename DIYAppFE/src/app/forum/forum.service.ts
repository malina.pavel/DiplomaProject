import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PostResponse } from '../shared/models/postResponse';
import { PostWithMetrics } from '../shared/models/postWithMetrics';
import { Comment } from '../shared/models/comment';
import { Post } from '../shared/models/post';
import { Like } from '../shared/models/like';

@Injectable({ providedIn: 'root' })
export class ForumService {
  private httpService: HttpClient;

  constructor(httpService: HttpClient) {
    this.httpService = httpService;
  }

  getPosts(pageNumber: number, pageSize: number): Observable<PostResponse> {
    return this.httpService.get<PostResponse>('https://localhost:7113/posts?pageNumber=' + pageNumber + '&pageSize=' + pageSize);
  }

  getPost(slug: string): Observable<PostWithMetrics> {
    return this.httpService.get<PostWithMetrics>('https://localhost:7113/posts/' + slug);
  }

  getNewestPosts(pageNumber: number, pageSize: number): Observable<PostResponse> {
    return this.httpService.get<PostResponse>('https://localhost:7113/posts/sort_newest?pageNumber=' + pageNumber + '&pageSize=' + pageSize);
  }

  getPopularPosts(pageNumber: number, pageSize: number): Observable<PostResponse> {
    return this.httpService.get<PostResponse>('https://localhost:7113/posts/sort_popular?pageNumber=' + pageNumber + '&pageSize=' + pageSize);
  }

  getPostsAlphabetically(pageNumber: number, pageSize: number): Observable<PostResponse> {
    return this.httpService.get<PostResponse>('https://localhost:7113/posts/sort_alphabetical?pageNumber=' + pageNumber + '&pageSize=' + pageSize);
  }

  getPostsByCategory(pageNumber: number, pageSize: number, category: string): Observable<PostResponse> {
    return this.httpService.get<PostResponse>('https://localhost:7113/posts/category/' + category + 
            '?pageNumber=' + pageNumber + '&pageSize=' + pageSize);
  }

  searchPosts(pageNumber: number, pageSize: number, keyword: string): Observable<PostResponse> {
    return this.httpService.get<PostResponse>('https://localhost:7113/posts/search?search=' + keyword + 
            '&pageNumber=' + pageNumber + '&pageSize=' + pageSize);
  }

  addPost(post: Post): Observable<Post> {
    return this.httpService.post<Post>('https://localhost:7113/posts/add_post', post);
  }

  getNewestComments(postId: string): Observable<Comment[]> {
    return this.httpService.get<Comment[]>('https://localhost:7113/posts/' + postId + '/comments/sort_newest');
  }

  addComment(postId: string, comment: Comment): Observable<Comment> {
    return this.httpService.post<Comment>('https://localhost:7113/posts/' + postId + '/comments/add_comment', comment);
  }

  getLikes(postId: string): Observable<Like[]> {
    return this.httpService.get<Like[]>('https://localhost:7113/posts/' + postId + '/likes');
  }

  increaseLikes(postId: string, like: Like): Observable<Like> {
    return this.httpService.post<Like>('https://localhost:7113/posts/' + postId + '/likes/increase_likes', like);
  }

  decreaseLikes(postId: string, userId: string): Observable<Like> {
    return this.httpService.delete<Like>('https://localhost:7113/posts/' + postId + '/likes/decrease_likes?userId=' + userId);
  }
}
