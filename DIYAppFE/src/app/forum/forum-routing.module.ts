import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForumComponent } from './forum.component';
import { ViewPostComponent } from './view-post/view-post.component';
import { PostEditorComponent } from './post-editor/post-editor.component';
import { AuthGuard } from '../shared/guard/auth.guard';

const routes: Routes = [
  { path: '', component: ForumComponent },
  { path: 'editor', component: PostEditorComponent, canActivate: [AuthGuard] },
  { path: ':slug', component: ViewPostComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ForumRoutingModule {}
