import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ForumService } from '../forum.service';
import { UserRegisteredService } from 'src/app/shared/services/user-registered.service';
import { Comment } from 'src/app/shared/models/comment';
import { UserSession } from 'src/app/shared/models/userSession';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-comment-section',
  templateUrl: './comment-section.component.html',
  styleUrls: ['./comment-section.component.css'],
})
export class CommentSectionComponent implements OnInit, OnChanges {
  @Input() postId: string = '';
  comments!: Comment[];
  partialComments!: Comment[];
  usernameMap: { [userId: string]: string } = {};
  userPictureMap: { [userId: string]: string } = {};
  private userIdLoggedIn!: string;
  isUserLoggedIn!: boolean;
  addComment: boolean = false;
  commentForm!: FormGroup;

  public Editor = ClassicEditor;
  public config = {
    toolbar: ['undo', 'redo', 
              '|', 'heading', 
              '|', 'bold', 'italic',
              '|', 'link', 'insertTable',
              '|', 'bulletedList', 'numberedList'
    ]
  }

  constructor(
    private forumService: ForumService,
    private userRegisteredService: UserRegisteredService,
    private formBuilder: FormBuilder
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    // when switching from one post to another, keep track of the changed postId from view-post
    // and reload comments specific to the new ID
    if(changes['postId'] && changes['postId'].currentValue != changes['postId'].previousValue) {
      this.loadComments(this.postId);
    }
  }

  ngOnInit(): void {
    this.userIdLoggedIn = this.userRegisteredService.getUserId();
    this.isUserLoggedIn = this.userIdLoggedIn != '' ? true : false;
    this.loadComments(this.postId);
    this.buildForm();
  }

  private buildForm() {
    this.commentForm = this.formBuilder.group({ comment: ['', Validators.required]});
  }

  private loadComments(postId: string) {
    this.forumService.getNewestComments(postId).subscribe((data: Comment[]) => {
      this.comments = data;
      this.mapUsers(data);
      this.partialComments = [];
      this.renderMoreComments(0, 5, this.comments); // initially rendering 5 comments
    })
  }

  private mapUsers(comments: Comment[]) {
    this.comments.forEach((comment: Comment) => {
      this.userRegisteredService.getUser(comment.userId).subscribe((data: UserSession) => {
        this.usernameMap[comment.userId] = data.username;
        this.userPictureMap[comment.userId] = data.profilePicture!;
      })
    })
  }

  postComment() {
    if(this.commentForm.valid) {
      const commentForm = this.commentForm.value;

      const comment: Comment = {
        postId: this.postId,
        userId: this.userIdLoggedIn,
        content: commentForm.comment
      }

      this.forumService.addComment(this.postId, comment).subscribe((data: Comment) => {
        this.addComment = false;
        this.loadComments(this.postId);
        this.commentForm.reset();
      })
    }
  }

  onScroll() {
    if(this.partialComments.length < this.comments.length) {
      this.renderMoreComments(this.partialComments.length, this.partialComments.length + 5, this.comments)
    }
  }

  private renderMoreComments(start: number, end: number, comments: Comment[]) {
    this.partialComments = this.partialComments.concat(comments.slice(start, end));
  }
}
