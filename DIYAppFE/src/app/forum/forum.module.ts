import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForumRoutingModule } from './forum-routing.module';
import { ForumComponent } from './forum.component';
import { ViewPostComponent } from './view-post/view-post.component';
import { PostEditorComponent } from './post-editor/post-editor.component';
import { ForumService } from './forum.service';
import { SharedModule } from '../shared/shared.module';
import { CommentSectionComponent } from './comment-section/comment-section.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';


@NgModule({
  declarations: [
    ForumComponent,
    ViewPostComponent,
    PostEditorComponent,
    CommentSectionComponent
  ],
  imports: [
    CommonModule,
    ForumRoutingModule,
    SharedModule,
    CKEditorModule,
    ReactiveFormsModule,
    InfiniteScrollModule
  ],
  providers: [ForumService]
})
export class ForumModule { }
