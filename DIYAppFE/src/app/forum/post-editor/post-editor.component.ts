import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DropdownCategories } from 'src/app/shared/enums/dropdown-categories';
import { UserSession } from 'src/app/shared/models/userSession';
import { ForumService } from '../forum.service';
import { UserRegisteredService } from 'src/app/shared/services/user-registered.service';
import { Router } from '@angular/router';
import { UploadImageService } from 'src/app/shared/services/upload-image.service';
import { Post } from 'src/app/shared/models/post';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-post-editor',
  templateUrl: './post-editor.component.html',
  styleUrls: ['./post-editor.component.css'],
})
export class PostEditorComponent implements OnInit {
  public Editor = ClassicEditor;
  public config = {
    toolbar: ['undo', 'redo', 
              '|', 'heading', 
              '|', 'bold', 'italic',
              '|', 'link', 'insertTable',
              '|', 'bulletedList', 'numberedList'
    ]
  }
  
  postForm!: FormGroup;
  dropdownCategories = Object.values(DropdownCategories);

  private userIdLoggedIn!: string;
  user!: UserSession;
  imageURL!: string;
  initialPostValues: any = {};

  private titleSource = new ReplaySubject<string>(1);
  titleSource$ = this.titleSource.asObservable();

  private descriptionSource = new ReplaySubject<string>(1);
  descriptionSource$ = this.descriptionSource.asObservable();

  private imageSource = new ReplaySubject<string>(1);
  imageSource$ = this.imageSource.asObservable();

  private categorySource = new ReplaySubject<string>(1);
  categorySource$ = this.categorySource.asObservable();

  constructor(
    private formBuilder: FormBuilder,
    private forumService: ForumService,
    private router: Router,
    private userRegisteredService: UserRegisteredService,
    private uploadImageService: UploadImageService
  ) {}

  ngOnInit(): void {
    this.userIdLoggedIn = this.userRegisteredService.getUserId();
    this.getUser();
    this.buildForm();
  }

  private getUser() {
    this.userRegisteredService.getUser(this.userIdLoggedIn).subscribe((data: UserSession) => {
        this.user = data;
    });
  }

  private buildForm() {
    this.postForm = this.formBuilder.group(
      {
        title: ['',[Validators.required, Validators.minLength(10)]],
        description: ['', [Validators.required, Validators.minLength(50)]],
        image: [null],
        category: ['', Validators.required]
      }
    );
    this.initialPostValues = {... this.postForm.value };
    this.postForm.valueChanges.subscribe((value) => {
      this.checkPostChanges();
    });
  }

  private checkPostChanges() {
    var currValues = this.postForm.value;

    Object.keys(currValues).forEach((key) => {
      if (currValues[key] != this.initialPostValues[key] && key != 'image') { // the image change will be handles separately
        this.initialPostValues[key] = currValues[key];
        console.log(key)
        switch(key) {
          case 'title':       this.titleSource.next(currValues[key]); break;
          case 'description': this.descriptionSource.next(currValues[key]); break;
          case 'category':    this.categorySource.next(currValues[key]); break;
          default: console.log("problem in detecting this change")
        }
      }
    });
  }

  publishPost() {
    if(this.postForm.valid) {
      const postForm = this.postForm.value;

      const post: Post = {
        userId: this.userIdLoggedIn,
        title: postForm.title,
        description: postForm.description,
        category: postForm.category,
        imageURL: this.imageURL != null ? this.imageURL : '',
        slug: ''
      }

      this.forumService.addPost(post).subscribe((post: Post) => {
        this.router.navigate(['/forum']);
        this.postForm.reset();
      })
    }
  }

  uploadImage(e: any, form: FormGroup) {
    const file = e.target.files[0];
    if (file) {
      this.checkImageSize(file, form);
    }
  }

  private checkImageSize(image: File, form: FormGroup) {
      const formData = new FormData();
      formData.append('imageFile', image);

      // if the image is bigger than 10MB, set error
      if(this.uploadImageService.imageSizeExceeded(image)) {
        form.get('image')?.setErrors({exceeded: true});
      }
      else { //success
        this.uploadImageService.uploadImage(formData).subscribe((response: any) => {
            console.log(response.imageURL);
            this.imageURL = response.imageURL;
            this.imageSource.next(this.imageURL);
        });
      }
  }
}
