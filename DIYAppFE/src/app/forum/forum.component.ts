import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ForumService } from './forum.service';
import { UserRegisteredService } from '../shared/services/user-registered.service';
import { DropdownCategories } from '../shared/enums/dropdown-categories';
import { PostResponse } from '../shared/models/postResponse';
import { Post } from '../shared/models/post';
import { UserSession } from '../shared/models/userSession';
import { PostWithMetrics } from '../shared/models/postWithMetrics';
import { Like } from '../shared/models/like';

@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.css'],
})
export class ForumComponent implements OnInit {
  posts!: Post[];
  usernameMap: { [userId: string]: string } = {};
  commentsMap: { [postId: string]: number } = {};
  likesMap: { [postId: string]: number } = {};
  isLikedByUser: { [postId: string]: boolean } = {};
  dropdownCategories = Object.values(DropdownCategories);
  isUserLoggedIn!: boolean;
  private userIdLoggedIn!: string;

  currentPage: number = 1;
  totalPosts: number = 0;
  totalPages: number = 0;
  totalPagesArray: number[] = [];
  pageSize: number = 5; // hardcoded for a better view on the UI
  actionType?: string;
  @ViewChild('searchInput') searchInputRef!: ElementRef;
  keyword!: string;
  categoryOption!: string;

  constructor(
    private forumService: ForumService,
    private userRegisteredService: UserRegisteredService
  ) {
    this.loadPosts(this.currentPage); // initially loading all the posts as they are
  }

  ngOnInit(): void {
    this.userIdLoggedIn = this.userRegisteredService.getUserId();
    this.isUserLoggedIn = this.userIdLoggedIn != '' ? true : false;
  }

  loadPosts(page: number): void {
    switch (this.actionType) {
      case 'popular': this.sortByPopularity(page); break;
      case 'latest':  this.sortByNewest(page); break;
      case 'alphabetically': this.sortAlphabetically(page); break;
      case 'category': this.getPostByCategory(page); break;
      case 'search': this.searchPost(page); break;
      default: this.getAllPosts(page);
    }
  }

  updateActionType(value: string) {
    this.actionType = value;
    if (this.searchInputRef.nativeElement.value) {
      this.keyword = this.searchInputRef.nativeElement.value;
      this.searchInputRef.nativeElement.value = '';
    }

    this.loadPosts(1); // start to display data from the first page
  }

  updateCategories(value: string, category: string) {
    this.actionType = value;
    this.categoryOption = category;
    this.loadPosts(1); // start to display data from the first page
  }

  private getAllPosts(page: number): void {
    this.forumService.getPosts(page, this.pageSize).subscribe((data: PostResponse) => {
      this.initialize(page, data);
    });
  }

  private sortByPopularity(page: number): void {
    this.forumService.getPopularPosts(page, this.pageSize).subscribe((data: PostResponse) => {
      this.initialize(page, data);
    });
  }

  private sortByNewest(page: number): void {
    this.forumService.getNewestPosts(page, this.pageSize).subscribe((data: PostResponse) => {
      this.initialize(page, data);
    });
  }

  private sortAlphabetically(page: number): void {
    this.forumService.getPostsAlphabetically(page, this.pageSize).subscribe((data: PostResponse) => {
      this.initialize(page, data);
    });
  }

  private getPostByCategory(page: number): void {
    this.forumService.getPostsByCategory(page, this.pageSize, this.categoryOption).subscribe((data: PostResponse) => {
      this.initialize(page, data);
    });
  }

  private searchPost(page: number): void {
    this.forumService.searchPosts(page, this.pageSize, this.keyword).subscribe((data: PostResponse) => {
      this.initialize(page, data);
    });
  }

  private initialize(page: number, response: PostResponse) {
    this.setPosts(response);
    this.computePageNumber(page, response.totalCount);
    window.scrollTo(0, 0);
  }

  private setPosts(response: PostResponse): void {
    this.posts = response.posts;
    this.mapMetrics(this.posts);
    this.getUser(this.posts);
  }

  private computePageNumber(page: number, totalCount: number) {
    this.totalPages = Math.ceil(totalCount / this.pageSize);
    this.totalPagesArray = new Array(this.totalPages);
    this.currentPage = page;
  }

  private getUser(posts: Post[]) {
    posts.forEach((post: Post) => {
      this.userRegisteredService.getUser(post.userId).subscribe((data: UserSession) => {
        this.usernameMap[post.userId] = data.username;
      });
    });
  }

  private mapMetrics(posts: Post[]) {
    posts.forEach((post: Post) => {
      if (post.slug && post.id) {
        var postSlug = post.slug;
        var postId = post.id;
        this.forumService.getPost(postSlug).subscribe((data: PostWithMetrics) => {
          this.commentsMap[postId] = data.comments.length;
          this.likesMap[postId] = data.like.length;
          this.loadUserLike(postId);
        });
      }
    });
  }

  private loadUserLike(postId: string) {
    this.forumService.getLikes(postId).subscribe((data: Like[]) => {
        const totalPostLikes = data;
        totalPostLikes.forEach((like: Like) => {
        if(like.userId == this.userIdLoggedIn)  this.isLikedByUser[postId] = true;
        });
    });
  }

  likePost(postSlug: string) {
    if (this.isUserLoggedIn) {
      this.forumService.getPost(postSlug).subscribe((post: PostWithMetrics) => {
        const like: Like = {
          userId: this.userIdLoggedIn,
          postId: post.id
        };
        this.isLikedByUser[post.id] = true;

        this.forumService.increaseLikes(post.id, like).subscribe((data: Like) => {
          this.refreshLikes(post.slug);
        });
      });
    }
  }

  dislikePost(postSlug: string) {
    if (this.isUserLoggedIn) {
      this.forumService.getPost(postSlug).subscribe((post: PostWithMetrics) => {
        this.isLikedByUser[post.id] = false;
        this.forumService.decreaseLikes(post.id, this.userIdLoggedIn).subscribe((data: Like) => {
          this.refreshLikes(post.slug);
        });
      });
    }
  }

  private refreshLikes(postSlug: string) {
    this.forumService.getPost(postSlug).subscribe((data: PostWithMetrics) => {
      this.likesMap[data.id] = data.like.length;
    });
  }
}
