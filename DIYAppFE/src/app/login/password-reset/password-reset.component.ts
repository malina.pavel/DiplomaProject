import { Component, OnInit } from '@angular/core';
import { AbstractControlOptions, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../login.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/shared/models/user';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.css']
})
export class PasswordResetComponent implements OnInit {
  updatePasswordForm!: FormGroup;
  email!: string;
  confirmationValid!: boolean;

  constructor(
    private formBuilder: FormBuilder, 
    private loginService: LoginService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.updatePasswordForm = this.formBuilder.group(
      {
        password: ['', [Validators.required, Validators.minLength(10)]],
        confirmPassword: ['', Validators.required],
      },
      { validators: this.passwordMatchValidator } as AbstractControlOptions
    );
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      const token = params['token'];
      this.email = params['email'];

      if (token && this.email) {
        this.loginService.confirmPasswordResetEmail(token, this.email).subscribe({
          next: (response) => this.confirmationValid = true,
          error: (err) => this.confirmationValid = false
        });
      }
    });
  }

  private passwordMatchValidator(form: FormGroup) {
    return form.get('password')!.value == form.get('confirmPassword')!.value ? null : form.get('confirmPassword')!.setErrors({ mismatch: true });
  }

  updatePassword() {
    if (this.updatePasswordForm.valid) {
      const password = this.updatePasswordForm.value.password;
      const patchDoc = [
        {op: 'replace', path: '/password', value: password},
        {op: 'replace', path: '/emailToken', value: "default"}
      ];

      this.loginService.updatePassword(this.email, patchDoc).subscribe((data: User) => {
        const toastLiveExample = document.getElementById('liveToast')

        if (toastLiveExample)   toastLiveExample.style.display = 'block';
        setTimeout(() => this.router.navigate(['/login']), 3000);
      });
    }
  }
}
