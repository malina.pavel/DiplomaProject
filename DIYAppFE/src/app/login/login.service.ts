import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { User } from "../shared/models/user";
import { Observable } from "rxjs";
import { AuthenticationResponse } from "../shared/models/authResponse";
import { UserLogin } from "../shared/models/userLogin";

@Injectable({providedIn: 'root'})

export class LoginService {
    private httpService: HttpClient;

    constructor(httpService: HttpClient) {
        this.httpService = httpService;
    }

    getUsers(): Observable<User[]> {
        return this.httpService.get<User[]>('https://localhost:7113/users/');
    }

    authUser(userLogin: UserLogin): Observable<AuthenticationResponse> {
        return this.httpService.post<AuthenticationResponse>('https://localhost:7113/users/login', userLogin, 
               { headers: new HttpHeaders({ "Content-Type": "application/json"}) });
    }

    sendPasswordResetEmail(email: string): Observable<User> {
        return this.httpService.get<User>('https://localhost:7113/users/password_reset_request?email=' + email);
    }

    confirmPasswordResetEmail(token: string, email: string) : Observable<User> {
        return this.httpService.get<User>('https://localhost:7113/users/replace_password_confirmation?token=' + token + '&email=' + email)
    }

    updatePassword(email: string, patch: any[]): Observable<User> {
        return this.httpService.patch<User>('https://localhost:7113/users/reset_password?email=' + email, patch);
    }
}