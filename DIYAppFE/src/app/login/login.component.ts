import { Component } from '@angular/core';
import { UserLogin } from '../shared/models/userLogin';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthenticationResponse } from '../shared/models/authResponse';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  public invalidLogin!: boolean;
  public credentials: UserLogin = { username: '', password: '' };

  type: string = 'password';
  isPassword: boolean = true;
  eyeIconType: string = 'bi bi-eye h5';
  errorMessage!: string;

  constructor(private router: Router, private loginService: LoginService, private jwtHelper: JwtHelperService) { }

  public login (form: NgForm) {
    if (form.valid) {
      this.loginService.authUser(this.credentials).subscribe({
          next: (response: AuthenticationResponse) => {
            const token = response.token;
            if(token){
              localStorage.setItem("jwt", token); 
              this.invalidLogin = false; 
              const decodedToken = this.jwtHelper.decodeToken(token);
              const role = decodedToken['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'];
              this.router.navigate(['/home']);
            } 
          },
          error: (error: HttpErrorResponse) => {
            this.displayErrorMessage(error);
            this.invalidLogin = true;
          }
      });
    }
  }

  private displayErrorMessage(error: HttpErrorResponse) {
    if(error.status == 500) {
      this.errorMessage = 'This account does not exist';
    }
    else this.errorMessage = error.error;
  }

  showHidePassword(): void {
    this.isPassword = !this.isPassword;
    this.isPassword ? this.type = 'password' : this.type = 'text';
    this.isPassword ? this.eyeIconType = 'bi bi-eye h5' : this.eyeIconType = 'bi bi-eye-slash h5';
  }

  resetInvalidState(): void {
    this.invalidLogin = false;
  }
}
