import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../login.service';
import { User } from 'src/app/shared/models/user';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {
  emailForm!: FormGroup;
  responseHeader!: string;
  responseMessage!: string

  constructor(private formBuilder: FormBuilder, private loginService: LoginService) {
    this.emailForm = this.formBuilder.group({ 
      email: ['', [Validators.required, Validators.pattern(/^[a-z0-9]+[._-]{0,1}[a-z0-9]+\@(gmail|yahoo|proton|outlook|aol).[a-z]{2,3}$/)]] 
    });
  }

  sendEmail() {
    if (this.emailForm.valid) {
      const email = this.emailForm.value.email;

      this.loginService.sendPasswordResetEmail(email).subscribe({
        next: (data:User) => {
          this.responseHeader = "Success";
          this.responseMessage = "Please check your inbox for changing your password";
        },
        error: (err: HttpErrorResponse) => {
          // this one is associated with email not being found
          if(err.status == 500) {
            this.responseHeader = "Fail"
            this.responseMessage = "There is no account associated to this user. Please try again";
          }   
        }
      })
    }
  }

  clearErrorMessage() {
    this.responseHeader = '';
    this.responseMessage = '';
  }
}
