import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrigamisComponent } from './origamis.component';
import { ViewOrigamiComponent } from './view-origami/view-origami.component';
import { OrigamiGameStepComponent } from './origami-game-step/origami-game-step.component';
import { AuthGuard } from '../shared/guard/auth.guard';
import { OrigamiGuard } from './origami-step.guard';

const routes: Routes = [
  { path: '', component: OrigamisComponent },
  { path: ':slug', component: ViewOrigamiComponent },
  { path: ':slug/:stepNumber', component: OrigamiGameStepComponent, canActivate: [AuthGuard, OrigamiGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrigamisRoutingModule {}
