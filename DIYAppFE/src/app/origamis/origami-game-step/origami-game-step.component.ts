import { Component, OnInit } from '@angular/core';
import { ReplaySubject, Subject, Subscription, of } from 'rxjs';
import { Origami } from 'src/app/shared/models/origami';
import { OrigamiService } from '../origamis.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserRegisteredService } from 'src/app/shared/services/user-registered.service';
import { OrigamiWithSteps } from 'src/app/shared/models/origamiWithSteps';
import { OrigamiStep } from 'src/app/shared/models/origamiStep';
import { WebcamImage, WebcamUtil } from 'ngx-webcam';
import { UploadImageService } from 'src/app/shared/services/upload-image.service';
import { PredictionResponse } from 'src/app/shared/models/predictionResponse';
import { ImagePredictionService } from 'src/app/shared/services/image-prediction.service';
import { HttpErrorResponse } from '@angular/common/http';
import { CustomVisionProject } from 'src/app/shared/models/customVisionProject';

@Component({
  selector: 'app-origami-game-step',
  templateUrl: './origami-game-step.component.html',
  styleUrls: ['./origami-game-step.component.css']
})
export class OrigamiGameStepComponent implements OnInit{
  routeSubscription!: Subscription;
  origamiSlug!: string;
  origami!: OrigamiWithSteps;
  origamiStep!: OrigamiStep;
  currentStep!: number;
  project!: CustomVisionProject;
  private predictionResultSource = new Subject<PredictionResponse | null>;
  predictionResultSource$ = this.predictionResultSource.asObservable();

  private triggerSource = new Subject<void>;
  triggerSource$ = this.triggerSource.asObservable();
  image!: string;
  imageURL!: string;

  isStepValid: boolean = false;

  constructor(
    private origamiService: OrigamiService,
    private userRegisteredService: UserRegisteredService,
    private uploadImageService: UploadImageService,
    private predictionService: ImagePredictionService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.routeSubscription = this.route.params.subscribe((params) => {
      this.origamiSlug = params['slug'];
      this.currentStep = parseInt(params['stepNumber'], 10);
      localStorage.setItem("currentStep", JSON.stringify(this.currentStep));
      this.loadOrigami(this.origamiSlug);
      this.imageURL = '';
      this.isStepValid = false;
    });
    
    WebcamUtil.getAvailableVideoInputs().then((mediaDevices: MediaDeviceInfo[]) => { });
  }

  loadOrigami(origamiSlug: string) {
    this.origamiService.getOrigamiModel(origamiSlug).subscribe((data: OrigamiWithSteps) => {
      this.origami = data;
      if(data.steps) this.origamiStep = data.steps[this.currentStep - 1];
      this.getProjectByName(this.origami.name);
    });
  }

  getProjectByName(name: string) {
    this.predictionService.getProjectByName(name).subscribe((data: CustomVisionProject) => {
      this.project = data;
    })
  }

  takePicture() {
    this.triggerSource.next();
  }

  async onCapture(e: WebcamImage) {
    // convert the imageAsDataUrl to blob
    // for storing it in Azure storage afterwards
    const blob = await (await fetch(e.imageAsDataUrl)).blob(); 
    const formData = new FormData();
    formData.append('imageFile', blob, this.buildImageName());
    this.uploadImageService.uploadImage(formData).subscribe((response: any) => {
      this.imageURL = response.imageURL;
      this.getPredictionResults(this.imageURL);
    });
    this.predictionResultSource.next(null);
  }

  private getPredictionResults(imageURL: string) {
    const tag = this.buildTagName();
    if(tag){
      this.predictionService.predictImageAlternative(this.project.id, imageURL, tag).subscribe({
        next: (data: PredictionResponse) => {
          this.isStepValid = true;
          this.predictionResultSource.next(data);
        },
        error: (err: HttpErrorResponse) => {
          this.isStepValid = false;
          this.predictionResultSource.next(err.error);
        }
      });
    }
  }

  onNextStep() {
    if(this.origami.steps && this.currentStep == this.origami.steps.length){
      this.router.navigate(['/origamis']);
    }
    else {
      this.router.navigate(['/origamis/', this.origami.slug, this.currentStep + 1]);
      window.scrollTo(0, 0);
    }
  }

  private buildImageName() {
    const timestamp = new Date().getTime();
    const name = "trial-origami-s" + this.currentStep + '-' + timestamp + '.png';
    return name;
  }

  private buildTagName() {
    const tag = this.origami.slug?.split("-origami")[0] + '-s' + this.currentStep;
    return tag;
  }
}
