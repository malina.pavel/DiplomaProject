import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrigamiGameStepComponent } from './origami-game-step.component';

describe('OrigamiGameStepComponent', () => {
  let component: OrigamiGameStepComponent;
  let fixture: ComponentFixture<OrigamiGameStepComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrigamiGameStepComponent]
    });
    fixture = TestBed.createComponent(OrigamiGameStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
