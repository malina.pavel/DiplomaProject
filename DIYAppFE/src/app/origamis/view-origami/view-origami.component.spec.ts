import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewOrigamiComponent } from './view-origami.component';

describe('ViewOrigamiComponent', () => {
  let component: ViewOrigamiComponent;
  let fixture: ComponentFixture<ViewOrigamiComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ViewOrigamiComponent]
    });
    fixture = TestBed.createComponent(ViewOrigamiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
