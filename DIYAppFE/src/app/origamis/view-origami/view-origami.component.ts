import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { OrigamiService } from '../origamis.service';
import { Origami } from 'src/app/shared/models/origami';
import { UserRegisteredService } from 'src/app/shared/services/user-registered.service';

@Component({
  selector: 'app-view-origami',
  templateUrl: './view-origami.component.html',
  styleUrls: ['./view-origami.component.css'],
})
export class ViewOrigamiComponent implements OnInit {
  routeSubscription!: Subscription;
  origamiSlug!: string;
  origami!: Origami;
  moreOrigamis!: Origami[];
  isUserLoggedIn!: boolean;

  constructor(
    private origamiService: OrigamiService,
    private userRegisteredService: UserRegisteredService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    const userIdLoggedIn = this.userRegisteredService.getUserId();
    this.isUserLoggedIn = userIdLoggedIn != '' ? true : false;
    this.routeSubscription = this.route.params.subscribe((params) => {
      this.origamiSlug = params['slug'];
      this.loadOrigami(this.origamiSlug);
    });
  }

  loadOrigami(origamiSlug: string) {
    this.origamiService.getOrigamiModel(origamiSlug).subscribe((data: Origami) => {
      this.origami = data;
      this.loadMoreOrigamis();
    });
  }

  loadMoreOrigamis() {
    this.origamiService.getOrigamiModels().subscribe((data: Origami[]) => {
      this.moreOrigamis = data;
      this.moreOrigamis = this.moreOrigamis.filter(a => a.slug != this.origami.slug);
    });
  }

  startGame() {
    if(this.isUserLoggedIn) {
      this.router.navigate(['/origamis/', this.origami.slug, 1]) // start from the first step
    }
  }
}
