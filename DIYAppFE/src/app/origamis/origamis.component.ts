import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Origami } from '../shared/models/origami';
import { OrigamiService } from './origamis.service';

@Component({
  selector: 'app-origamis',
  templateUrl: './origamis.component.html',
  styleUrls: ['./origamis.component.css']
})
export class OrigamisComponent implements OnInit{
  origamis!: Origami[];

  @ViewChild('searchInput') searchInputRef!: ElementRef;
  keyword!: string;
  
  constructor(private origamiService: OrigamiService) { }

  ngOnInit(): void {
    this.loadOrigamis();
  }

  loadOrigamis() {
    this.origamiService.getOrigamiModels().subscribe((data: Origami[]) => {
      this.origamis = data;
    });
  }

  searchOrigamis() {
    if(this.searchInputRef.nativeElement.value) {
      this.keyword = this.searchInputRef.nativeElement.value;
      this.searchInputRef.nativeElement.value='';
    }

    this.origamiService.searchOrigamiModel(this.keyword).subscribe((data: Origami[]) => {
      this.origamis = data;
    });
  }
}
