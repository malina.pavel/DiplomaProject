import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Origami } from "../shared/models/origami";
import { OrigamiWithSteps } from "../shared/models/origamiWithSteps";

@Injectable({providedIn: 'root'})
export class OrigamiService {
    private httpService: HttpClient;

    constructor(httpService: HttpClient) {
        this.httpService = httpService;
    }

    getOrigamiModels(): Observable<Origami[]> {
        return this.httpService.get<Origami[]>('https://localhost:7113/origamis/');
    }

    getOrigamiModel(origamiSlug: string): Observable<Origami> {
        return this.httpService.get<Origami>('https://localhost:7113/origamis/' + origamiSlug);
    }

    getOrigamiModelWithSteps(origamiSlug: string): Observable<OrigamiWithSteps> {
        return this.httpService.get<OrigamiWithSteps>('https://localhost:7113/origamis/' + origamiSlug);
    }

    searchOrigamiModel(keyword: string): Observable<Origami[]> {
        return this.httpService.get<Origami[]>('https://localhost:7113/origamis/search?search=' + keyword);
    }
}