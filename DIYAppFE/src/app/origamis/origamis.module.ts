import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrigamisRoutingModule } from './origamis-routing.module';
import { OrigamisComponent } from './origamis.component';
import { ViewOrigamiComponent } from './view-origami/view-origami.component';
import { OrigamiGameStepComponent } from './origami-game-step/origami-game-step.component';
import { OrigamiService } from './origamis.service';
import { SharedModule } from '../shared/shared.module';
import { WebcamModule } from 'ngx-webcam';


@NgModule({
  declarations: [
    OrigamisComponent,
    ViewOrigamiComponent,
    OrigamiGameStepComponent
  ],
  imports: [
    CommonModule,
    OrigamisRoutingModule,
    SharedModule,
    WebcamModule
  ],
  providers: [OrigamiService]
})
export class OrigamisModule { }
