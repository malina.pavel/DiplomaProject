import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrigamisComponent } from './origamis.component';

describe('OrigamisComponent', () => {
  let component: OrigamisComponent;
  let fixture: ComponentFixture<OrigamisComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrigamisComponent]
    });
    fixture = TestBed.createComponent(OrigamisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
