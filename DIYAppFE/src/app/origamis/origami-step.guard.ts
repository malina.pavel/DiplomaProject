import { Injectable, inject } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateFn,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class OrigamiGuardService implements CanActivate {
  constructor(private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentStep = parseInt(next.params['stepNumber']);
    const origamiSlug = next.params['slug'];
    const local = localStorage.getItem('currentStep');

    if (local && currentStep > 1) {
      const localStep = parseInt(local);
      if((currentStep > localStep + 1) || (currentStep < localStep) ){
        this.router.navigate(['/origamis', origamiSlug, localStep]);
        return false;
      }
    }

    return true;
  }
}
export const OrigamiGuard: CanActivateFn = (next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean => {
    return inject(OrigamiGuardService).canActivate(next, state);
}
