import { NgModule } from '@angular/core';
import { UserProfileModalComponent } from './components/user-profile-modal/user-profile-modal.component';
import { DateTimeFormatPipe } from './pipes/date-time.pipe';
import { TimeDifferencePipe } from './pipes/time-difference.pipe';
import { DomSanitizerPipe } from './pipes/dom-sanitizer.pipe';
import { CommonModule } from '@angular/common';
import { UserUpdateService } from './services/user-update.service';
import { ActionConfirmationComponent } from './components/action-confirmation/action-confirmation.component';
import { UserRegisteredService } from './services/user-registered.service';
import { UploadImageService } from './services/upload-image.service';
import { AccountNeededModalComponent } from './components/account-needed-modal/account-needed-modal.component';
import { RouterModule } from '@angular/router';
import { ImagePredictionService } from './services/image-prediction.service';
import { ConvertToStepNumberPipe } from './pipes/origami-step-nr';

@NgModule({
  declarations: [
    UserProfileModalComponent,
    DateTimeFormatPipe,
    TimeDifferencePipe,
    DomSanitizerPipe,
    ConvertToStepNumberPipe,
    ActionConfirmationComponent,
    AccountNeededModalComponent,
  ],
  imports: [CommonModule, RouterModule],
  exports: [
    UserProfileModalComponent,
    ActionConfirmationComponent,
    DateTimeFormatPipe,
    TimeDifferencePipe,
    DomSanitizerPipe,
    ConvertToStepNumberPipe,
    AccountNeededModalComponent,
  ],
  providers: [
    UserUpdateService,
    UserRegisteredService,
    UploadImageService,
    ImagePredictionService,
  ],
})
export class SharedModule {}
