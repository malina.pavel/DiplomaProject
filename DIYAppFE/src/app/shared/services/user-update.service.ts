import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserSession } from '../models/userSession';

@Injectable({ providedIn: 'root' })

// used mainly for updating the profile picture of the header in real time after profile editing
export class UserUpdateService {
  private userUpdateSource = new BehaviorSubject<UserSession | null>(null);
  userUpdateSource$ = this.userUpdateSource.asObservable();

  constructor() {}

  updateUser(user: UserSession) {
    this.userUpdateSource.next(user);
  }
}
