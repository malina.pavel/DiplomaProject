import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserSession } from '../models/userSession';
import { User } from '../models/user';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UserRegisteredService {
  private httpService: HttpClient;

  constructor(httpService: HttpClient, private jwtHelper: JwtHelperService) {
    this.httpService = httpService;
  }

  getUserId(): string {
    const token = localStorage.getItem('jwt');
    if (token) {
      var decodedToken = this.jwtHelper.decodeToken(token);
      return decodedToken['nameid'];
    }
    return '';
  }

  getUserRole(): string {
    const token = localStorage.getItem('jwt');
    if (token) {
      var decodedToken = this.jwtHelper.decodeToken(token);
      return decodedToken['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'];
    }
    return '';
  }

  getUser(userId: string): Observable<UserSession> {
    return this.httpService.get<User>('https://localhost:7113/users/' + userId);
  }
}
