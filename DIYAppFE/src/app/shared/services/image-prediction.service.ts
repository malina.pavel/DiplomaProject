import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PredictionResponse } from '../models/predictionResponse';
import { CustomVisionProject } from '../models/customVisionProject';

@Injectable({ providedIn: 'root' })
export class ImagePredictionService {
  private httpService: HttpClient;

  constructor(httpService: HttpClient) {
    this.httpService = httpService;
  }

  predictImage(imageURL: string, requestTag: string): Observable<PredictionResponse> {
    return this.httpService.get<PredictionResponse>('https://localhost:7113/prediction?imageUrl=' + imageURL + 
            '&requestTag=' + requestTag);
  }

  predictImageAlternative(projectId: string, imageURL: string, requestTag: string): Observable<PredictionResponse> {
    return this.httpService.get<PredictionResponse>('https://localhost:7113/prediction/prediction_result?projectId=' + projectId + 
            '&imageUrl=' + imageURL + '&requestTag=' + requestTag);
  }

  getProjectByName(name: string): Observable<CustomVisionProject> {
    return this.httpService.get<CustomVisionProject>('https://localhost:7113/training/get_project_name?name=' + name);
  }
}
