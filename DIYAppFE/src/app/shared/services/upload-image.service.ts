import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UploadImageService {
  private httpService: HttpClient;
  private MAX_SIZE = 10 * 1024 * 1024; // maximum image size admitted

  constructor(httpService: HttpClient) {
    this.httpService = httpService;
  }

  // upload profile picture to Azure Blob Storage
  uploadImage(image: FormData): Observable<string> {
    return this.httpService.post<string>('https://localhost:7113/upload-image', image);
  }

  imageSizeExceeded(image: File) {
    if (image.size > this.MAX_SIZE) {
        return true;
    } 
    return false;
  }
}
