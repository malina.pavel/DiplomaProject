export interface OrigamiStep {
    id?: string;
    origamiId?: string;
    stepNumber: number;
    description: string;
    imageURL: string;
}