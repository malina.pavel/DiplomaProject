import { Article } from "./article";

export interface ArticleResponse {
    totalCount: number;
    articles: Article[];
}