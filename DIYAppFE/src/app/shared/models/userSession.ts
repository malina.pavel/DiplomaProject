export interface UserSession {
    id?: string;
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    profilePicture?: string;
    userRole: string;
}