export interface ArticleStep {
    id?: string;
    articleId: string;
    stepNumber: number;
    title: string;
    overview: string;
    imageURL?: string;
}