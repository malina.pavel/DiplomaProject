export interface User {
    id?: string;
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    passwordSalt?: string;
    profilePicture?: string;
    userRole: string;
    emailToken?: string;
    emailConfirmed?: boolean
}