import { OrigamiStep } from "./origamiStep";

export interface OrigamiWithSteps {
    id?: string;
    name: string;
    overview: string;
    thumbnail: string;
    slug?: string;
    steps?: OrigamiStep[];
}