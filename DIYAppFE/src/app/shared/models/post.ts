export interface Post {
    id?: string;
    userId: string;
    title: string;
    description: string;
    category: string;
    imageURL?: string;
    createdDate?: string;
    likes?: number;
    slug?: string;
}