export interface CustomVisionProject {
  id: string;
  name: string;
  description: string;
  settings: Settings;
  created: string;
  lastModified: string;
  thumbnailUri: string;
  drModeEnabled: boolean;
  status: string;
}

export interface Settings {
  domainId: string;
  classificationType: string;
  targetExportPlatforms: any[];
  useNegativeSet: boolean;
  detectionParameters: any;
  imageProcessingSettings: ImageProcessingSettings;
}

export interface ImageProcessingSettings {
  augmentationMethods: AugmentationMethods;
}

export interface AugmentationMethods {
  rotation: boolean;
  scaling: boolean;
  translation: boolean;
  'horizontal flip': boolean;
  equalize: boolean;
  solarize: boolean;
  padtosquare: boolean;
}
