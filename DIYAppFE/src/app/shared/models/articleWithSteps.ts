import { ArticleStep } from "./articleStep";

export interface ArticleWithSteps {
    id?: string;
    userId: string;
    title: string;
    overview: string;
    category: string;
    thumbnail: string;
    views?: number;
    createdDate?: string;
    publishedDate?: string;
    slug?: string;
    isPublished: boolean;
    steps?: ArticleStep[];
}