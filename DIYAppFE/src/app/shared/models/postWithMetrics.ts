import { Comment } from './comment';
import { Like } from './like';

export interface PostWithMetrics {
  id: string;
  userId: string;
  title: string;
  description: string;
  category: string;
  imageURL: string;
  createdDate: string;
  likes: number;
  slug: string;
  comments: Comment[];
  like: Like[];
}
