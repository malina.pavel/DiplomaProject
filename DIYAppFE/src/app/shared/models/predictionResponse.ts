export interface PredictionResponse {
    probability: number;
    tag: string;
    message: string;
}