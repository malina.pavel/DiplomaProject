import { Post } from "./post";

export interface PostResponse {
    totalCount: number;
    posts: Post[];
}