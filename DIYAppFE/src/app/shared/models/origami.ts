export interface Origami {
    id?: string;
    name: string;
    overview: string;
    thumbnail: string;
    slug?: string;
}