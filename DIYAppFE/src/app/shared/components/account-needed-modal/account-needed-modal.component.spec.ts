import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountNeededModalComponent } from './account-needed-modal.component';

describe('AccountNeededModalComponent', () => {
  let component: AccountNeededModalComponent;
  let fixture: ComponentFixture<AccountNeededModalComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AccountNeededModalComponent]
    });
    fixture = TestBed.createComponent(AccountNeededModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
