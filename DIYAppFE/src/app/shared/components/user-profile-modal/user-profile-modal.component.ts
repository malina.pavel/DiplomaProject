import { Component } from '@angular/core';

@Component({
  selector: 'app-user-profile-modal',
  templateUrl: './user-profile-modal.component.html',
  styleUrls: ['./user-profile-modal.component.css'],
})
export class UserProfileModalComponent {}
