import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'convertToStepNr' })
export class ConvertToStepNumberPipe implements PipeTransform {
  transform(value: string) {
    return "step #" + value.slice(-1);
  }
}