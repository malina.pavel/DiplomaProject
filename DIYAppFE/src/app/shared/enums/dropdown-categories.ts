export enum DropdownCategories {
    woodworking = "Woodworking",
    metalworking = "Metalworking",
    gardening = "Gardening",
    electronics = "Electronics",
    homeRenovation = "Home Renovation",
    sewingTextiles = "Sewing & Textiles",
    accessories = "Accessories Crafting",
    pottery = "Pottery & Ceramics",
    paper = "Paper Crafting",
    cooking = "Cooking & Baking",
    upcycling = "Upcycling"
}