import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { HomeModule } from './home/home.module';
import { LoginModule } from './login/login.module';
import { RegistrationModule } from './registration/registration.module';
import { AboutUsModule } from './about-us/about-us.module';
import { ArticlesModule } from './articles/articles.module';
import { ForumModule } from './forum/forum.module';
import { UserProfileModule } from './user-profile/user-profile.module';
import { OrigamisModule } from './origamis/origamis.module';
import { DraftModule } from './draft/draft.module';
import { FormsModule } from '@angular/forms';
import { UserUpdateService } from './shared/services/user-update.service';
import { SharedModule } from './shared/shared.module';
import { AdminModule } from './admin/admin.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    CoreModule,
    HomeModule,
    LoginModule,
    RegistrationModule,
    AboutUsModule,
    ArticlesModule,
    ForumModule,
    OrigamisModule,
    UserProfileModule,
    DraftModule,
    AdminModule,
    SharedModule
  ],
  providers: [UserUpdateService],
  bootstrap: [AppComponent],
})
export class AppModule {}
