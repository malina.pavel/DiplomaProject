import { Component, OnInit } from '@angular/core';
import { UserSession } from '../shared/models/userSession';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { Article } from '../shared/models/article';
import { UserProfileService } from './user-profile.service';
import { Post } from '../shared/models/post';
import { UserRegisteredService } from '../shared/services/user-registered.service';
import { PostResponse } from '../shared/models/postResponse';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
})
export class UserProfileComponent implements OnInit{
  private userIdLoggedIn!: string;
  user?: UserSession;
  userArticles: Article[] = [];
  userPosts: Post[] = [];
  recentArticle?: Article;
  popularArticle?: Article;

  constructor(
    private userProfileService: UserProfileService,
    private userRegisteredService: UserRegisteredService
  ) { }

  ngOnInit(): void {
    this.userIdLoggedIn = this.userRegisteredService.getUserId();
    this.getUser();
    this.getUserArticles(this.userIdLoggedIn);
    this.getUserPosts(this.userIdLoggedIn);
  }

  private getUser(): void {
    this.userRegisteredService.getUser(this.userIdLoggedIn).subscribe((data: UserSession) => {
        this.user = data;
    });
  }

  private getUserArticles(userId: string): void {
    this.userProfileService.getArticles().subscribe((data: Article[]) => {
        const publishedArticles = data.filter(p => p.isPublished == true);

        publishedArticles.forEach((article: Article) => {
            if(article.userId == userId) {
              this.userArticles.push(article);
            }
        });

        this.recentArticle = this.userArticles.sort((a,b) => {
          const dateA = a.publishedDate || '00000000-0000-0000-0000-000000000000'
          const dateB = b.publishedDate || '00000000-0000-0000-0000-000000000000'
          return dateB.localeCompare(dateA);
        })[0];

        this.popularArticle = this.userArticles.sort((a,b) => {
          const viewsA = a.views || 0;
          const viewsB = b.views || 0;
          return viewsB - viewsA
        })[0];
    });
  }

  private getUserPosts(userId: string): void {
    var totalCount;
    this.userProfileService.getPublishedPosts(1,5).subscribe((data: PostResponse) => {
        totalCount = data.totalCount; // get the total number of posts first
        this.getPostsByPagination(totalCount, userId)
    });
  }

  private getPostsByPagination(count: number, userId: string) {
    this.userProfileService.getPublishedPosts(1, count).subscribe((data: PostResponse) => { 
      const publishedPosts = data.posts;
      publishedPosts.forEach((post: Post) => {
        if(post.userId == userId) {
          this.userPosts.push(post);
        }
      });
    });
  }
}
