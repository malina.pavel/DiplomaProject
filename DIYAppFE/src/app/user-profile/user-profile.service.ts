import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, ObservedValueOf } from "rxjs";
import { UserSession } from "../shared/models/userSession";
import { User } from "../shared/models/user";
import { Article } from "../shared/models/article";
import { Post } from "../shared/models/post";
import { PostResponse } from "../shared/models/postResponse";

@Injectable({providedIn: 'root'})

export class UserProfileService { 
    private httpService: HttpClient;

    constructor(httpService: HttpClient) {
        this.httpService = httpService;
    }

    getArticles(): Observable<Article[]> {
        return this.httpService.get<Article[]>('https://localhost:7113/articles/');
    }

    getPublishedPosts(pageNumber: number, pageSize: number): Observable<PostResponse> {
        return this.httpService.get<PostResponse>('https://localhost:7113/posts/?pageNumber=' + pageNumber + '&pageSize=' + pageSize);
    }

    updateProfile(userId: string, patch: any[]): Observable<User> {
        return this.httpService.patch<User>('https://localhost:7113/users/update_user?userId=' + userId, patch)
    }
}
