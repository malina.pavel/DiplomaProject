import { Component, OnInit } from '@angular/core';
import { UserProfileService } from '../user-profile.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { UserSession } from 'src/app/shared/models/userSession';
import {
  AbstractControlOptions,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { User } from 'src/app/shared/models/user';
import { UserUpdateService } from 'src/app/shared/services/user-update.service';
import { UserRegisteredService } from 'src/app/shared/services/user-registered.service';
import { UploadImageService } from 'src/app/shared/services/upload-image.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css'],
})
export class EditProfileComponent implements OnInit{
  updateForm!: FormGroup;
  private userIdLoggedIn!: string;
  user?: UserSession;
  previousValues: any = {};
  patchBuilder: any = [];
  imageURLAzure!: string;
  imageSizeError: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private userProfileService: UserProfileService,
    private router: Router,
    private userUpdateService: UserUpdateService,
    private userRegisteredService: UserRegisteredService,
    private uploadImageService: UploadImageService
  ) { }

  ngOnInit(): void {
     this.userIdLoggedIn = this.userRegisteredService.getUserId();
     this.initialization();
  }

  initialization(): void {
    this.userRegisteredService.getUser(this.userIdLoggedIn).subscribe((data: UserSession) => {
      this.user = data;
      // placing the form builder here
      // so that it can make use of user's property for setting the default field values
      this.buildForm();
      this.previousValues = { ...this.updateForm.value };
      this.patchBuilder = [];
      this.updateForm.valueChanges.subscribe((value) => {
        this.checkChanges();
      });
    });
  }

  private buildForm() {
    this.updateForm = this.formBuilder.group(
      {
        firstName: [
          this.user?.firstName,
          [Validators.required, Validators.maxLength(20)],
        ],
        lastName: [
          this.user?.lastName,
          [Validators.required, Validators.maxLength(20)],
        ],
        email: [this.user?.email, [Validators.required, Validators.email]],
        username: [
          this.user?.username,
          [Validators.required, Validators.maxLength(20)],
        ],
      }
    );
  }

  private checkChanges(): void {
    var currValues = this.updateForm.value;

    Object.keys(currValues).forEach((key) => {
      if (currValues[key] != this.previousValues[key]) {
        this.previousValues[key] = currValues[key];
        this.patchBuilder = this.patchBuilder.filter(
          (patch: { path: string }) => patch.path !== `/${key}`
        );
        this.patchBuilder.push({
          op: 'replace',
          path: `/${key}`,
          value: currValues[key],
        });
      }
    });
  }

  updateUser(): void {
    this.userProfileService.updateProfile(this.userIdLoggedIn, this.patchBuilder).subscribe((data: User) => {
        next: {
          this.imageSizeError = false;
          this.fetchUpdatedUser();
          this.updateForm.reset();
          this.updateForm.enable();
          this.initialization();
          this.router.navigate(['/profile']);
        }
        error: () => {
          console.log('Error on updating user data!');
          this.updateForm.enable();
        };
      });
  }

  uploadProfilePicture(e: any) {
    this.imageSizeError = false;
    const file = e.target.files[0];
    if (file) {
      const formData = new FormData();
      formData.append('imageFile', file);

      if(this.uploadImageService.imageSizeExceeded(file)){
        this.imageSizeError = true;
      }
      else{
        this.uploadImageService.uploadImage(formData).subscribe((response: any) => {
          console.log(response.imageURL);
          this.imageURLAzure = response.imageURL;
          this.patchBuilder = this.patchBuilder.filter((patch: { path: string }) => patch.path !== '/profilePicture');
          this.patchBuilder.push({
            op: 'replace',
            path: '/profilePicture',
            value: this.imageURLAzure,
          });
        });
      }
    }
  }

  removeProfilePicture() {
    // default profile picture set
    this.imageURLAzure = 'https://diyproject.blob.core.windows.net/diyproject/empty pfp.PNG';
    this.user!.profilePicture = this.imageURLAzure;
    this.patchBuilder = this.patchBuilder.filter((patch: { path: string }) => patch.path !== '/profilePicture');
          this.patchBuilder.push({
            op: 'replace',
            path: '/profilePicture',
            value: this.user!.profilePicture,
    });
  }

  private fetchUpdatedUser() {
    this.userRegisteredService.getUser(this.userIdLoggedIn).subscribe((data: UserSession) => {
      this.user = data;
      this.userUpdateService.updateUser(data);
    });
  }
}
