import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserProfileRoutingModule } from './user-profile-routing.module';
import { UserProfileComponent } from './user-profile.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { UserProfileService } from './user-profile.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserUpdateService } from '../shared/services/user-update.service';
import { SharedModule } from '../shared/shared.module';
import { UserRegisteredService } from '../shared/services/user-registered.service';
import { UploadImageService } from '../shared/services/upload-image.service';

@NgModule({
  declarations: [UserProfileComponent, EditProfileComponent],
  imports: [
    CommonModule,
    UserProfileRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [
    UserProfileService,
    UserUpdateService,
    UserRegisteredService,
    UploadImageService,
  ],
})
export class UserProfileModule {}
