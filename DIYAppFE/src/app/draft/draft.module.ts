import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DraftRoutingModule } from './draft-routing.module';
import { DraftComponent } from './draft.component';
import { DraftService } from './draft.service';
import { SharedModule } from '../shared/shared.module';
import { UserRegisteredService } from '../shared/services/user-registered.service';


@NgModule({
  declarations: [
    DraftComponent
  ],
  imports: [
    CommonModule,
    DraftRoutingModule,
    SharedModule
  ],
  providers:[DraftService, UserRegisteredService]
})
export class DraftModule { }
