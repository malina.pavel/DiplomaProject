import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Article } from '../shared/models/article';
import { ArticleWithSteps } from '../shared/models/articleWithSteps';
import { ArticleStep } from '../shared/models/articleStep';

@Injectable({ providedIn: 'root' })
export class DraftService {
  private httpService: HttpClient;

  constructor(httpService: HttpClient) {
    this.httpService = httpService;
  }

  getDrafts(userId: string): Observable<Article[]> {
    return this.httpService.get<Article[]>('https://localhost:7113/articles/drafts/' + userId);
  }

  getDraft(slug: string): Observable<ArticleWithSteps> {
    return this.httpService.get<ArticleWithSteps>('https://localhost:7113/articles/' + slug);
  }

  publishDrafts(draftId: string): Observable<Article> {
    const patch = [
      {
        op: 'replace',
        path: '/isPublished',
        value: true,
      },
      {
        op: 'replace',
        path: '/views',
        value: 0,
      }
    ];
    return this.httpService.patch<Article>('https://localhost:7113/articles/update_article?articleId=' + draftId, patch);
  };

  deleteDrafts(draftId: string) : Observable<Article> {
    return this.httpService.delete<Article>('https://localhost:7113/articles/delete_article?articleId=' + draftId);
  }

  deleteArticleStep(draftId: string, stepId: string): Observable<ArticleStep> {
    return this.httpService.delete<ArticleStep>('https://localhost:7113/articles/' + draftId + '/steps/' + stepId + '/delete_step');
  }
}
