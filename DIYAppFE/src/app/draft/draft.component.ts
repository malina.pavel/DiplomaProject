import { Component, OnInit } from '@angular/core';
import { Article } from '../shared/models/article';
import { DraftService } from './draft.service';
import { ArticleWithSteps } from '../shared/models/articleWithSteps';
import { ArticleStep } from '../shared/models/articleStep';
import { HttpErrorResponse } from '@angular/common/http';
import { UserRegisteredService } from '../shared/services/user-registered.service';

@Component({
  selector: 'app-draft',
  templateUrl: './draft.component.html',
  styleUrls: ['./draft.component.css']
})
export class DraftComponent implements OnInit{
  drafts!: Article[];
  private userIdLoggedIn!: string;
  
  constructor(
    private draftService: DraftService, 
    private userRegisteredService: UserRegisteredService
  ) { }

  ngOnInit(): void {
    this.userIdLoggedIn = this.userRegisteredService.getUserId();
    this.getDrafts();
  }

  private getDrafts() {
    this.draftService.getDrafts(this.userIdLoggedIn).subscribe((data: Article[]) => {
      this.drafts = data;
    });
  }

  publishDraft(draftId: string) {
    this.draftService.publishDrafts(draftId).subscribe((data: Article) => {
      this.getDrafts(); // refresh table
    })
  }

  deleteDraft(draftSlug: string) {
    this.draftService.getDraft(draftSlug).subscribe((data: ArticleWithSteps) => {
      // cascade delete all associated steps
      if(data.steps){
        this.deleteArticleSteps(data.steps);
      }
      if(data.id) {
          this.draftService.deleteDrafts(data.id).subscribe((data:Article) => {
            this.getDrafts(); // refresh table
          })
      }
    });
  }

  private deleteArticleSteps(steps: ArticleStep[]) {
    steps.forEach((step: ArticleStep) => {
      if(step.id) {
        this.draftService.deleteArticleStep(step.articleId, step.id).subscribe({
          next: (data:ArticleStep) => console.log('deleted'),
          error: (err: HttpErrorResponse) => console.log(err)
        });
      }
    })
  }
}
