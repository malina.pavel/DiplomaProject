import { Component, OnInit } from '@angular/core';
import { RegistrationService } from '../registration.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-confirm-mail',
  templateUrl: './confirm-mail.component.html',
  styleUrls: ['./confirm-mail.component.css'],
})
export class ConfirmMailComponent implements OnInit {
  confirmationMessage!: string;

  constructor(
    private registrationService: RegistrationService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      const token = params['token'];
      const email = params['email'];

      if (token && email) {
        this.registrationService.confirmEmail(token, email).subscribe({
            next: (response) => {
              this.confirmationMessage = 
              `<div class="d-flex justify-content-center align-items-center">
                  <h1 class="mt-5 pt-5 text-center" style="letter-spacing: 1px;"> Your account has been activated! </h1>
               </div>
               <p class="mt-3 pt-3 text-center"> You will be redirected to the login page soon...</p>`;
               setTimeout(() => this.router.navigate(['/login']), 5000);
            },
            error: (error) => {
              this.confirmationMessage = 
              `<div class="d-flex justify-content-center align-items-center">
                 <h1 class="mt-5 pt-5 text-center" style="letter-spacing: 1px;"> Something went wrong... </h1>
              </div>
              <p class="mt-3 pt-3 text-center"> Please contact support for help. </p>`;
          }
        });
      } 
      else {
        this.confirmationMessage = `<p class="mt-3 pt-3 text-center"> Invalid token or email! </p>`;
      }
    });
  }
}
