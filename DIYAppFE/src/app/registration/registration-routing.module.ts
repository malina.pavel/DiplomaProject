import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistrationComponent } from './registration.component';
import { RegistrationSuccessComponent } from './registration-success/registration-success.component';
import { ConfirmMailComponent } from './confirm-mail/confirm-mail.component';

const routes: Routes = [
  { path: '', component: RegistrationComponent },
  { path: 'registration-success', component: RegistrationSuccessComponent },
  { path: 'confirm-email', component: ConfirmMailComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistrationRoutingModule {}
