import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegistrationRoutingModule } from './registration-routing.module';
import { RegistrationComponent } from './registration.component';
import { RegistrationService } from './registration.service';
import { RegistrationSuccessComponent } from './registration-success/registration-success.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmMailComponent } from './confirm-mail/confirm-mail.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    RegistrationComponent,
    RegistrationSuccessComponent,
    ConfirmMailComponent
  ],
  imports: [
    CommonModule,
    RegistrationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [
    RegistrationService
  ]
})
export class RegistrationModule { }
