import { Injectable } from "@angular/core";
import { User } from "../shared/models/user";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({providedIn: 'root'})

export class RegistrationService {
    private httpService: HttpClient;

    constructor(httpService: HttpClient) {
        this.httpService = httpService;
    }

    
    saveUser(userData: User): Observable<User> {
        console.log(userData)
        return this.httpService.post<User>('https://localhost:7113/users/add_user', userData);
    }

    confirmEmail(token: string, email: string) {
        return this.httpService.get('https://localhost:7113/users/confirm_email' + '?token=' + token + '&email=' + email);
    }
}