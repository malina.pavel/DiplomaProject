import { Component } from '@angular/core';
import { User } from '../shared/models/user';
import { RegistrationService } from './registration.service';
import { Router } from '@angular/router';
import {
  AbstractControlOptions,
  FormBuilder,
  FormGroup,
  NgForm,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent {
  registrationForm!: FormGroup;
  existentEmailError!: string;
  existentUsernameError!: string;

  constructor(
    private formBuilder: FormBuilder,
    private registrationService: RegistrationService,
    private router: Router
  ) {
    this.registrationForm = this.formBuilder.group(
      {
        firstName: ['', [Validators.required, Validators.maxLength(20)]],
        lastName: ['', [Validators.required, Validators.maxLength(20)]],
        email: ['', [Validators.required, Validators.pattern(/^[a-z0-9]+[._-]{0,1}[a-z0-9]+\@(gmail|yahoo|proton|outlook|aol).[a-z]{2,3}$/)]],
        username: ['', [Validators.required, Validators.maxLength(20)]],
        password: ['', [Validators.required, Validators.minLength(10)]],
        confirmPassword: ['', Validators.required],
        userRole: ['', Validators.required],
        termsAccepted: [false, Validators.requiredTrue],
      },
      { validators: this.passwordMatchValidator } as AbstractControlOptions
    );
  }

  private passwordMatchValidator(form: FormGroup) {
    return form.get('password')!.value == form.get('confirmPassword')!.value ? null : form.get('confirmPassword')!.setErrors({ mismatch: true });
  }

  register(): void {
    if (this.registrationForm.valid) {
      const userCredentials = this.registrationForm.value;
      const user: User = {
        firstName: userCredentials.firstName,
        lastName: userCredentials.lastName,
        email: userCredentials.email,
        username: userCredentials.username,
        password: userCredentials.password,
        passwordSalt: '',
        userRole: userCredentials.userRole,
        emailConfirmed: false,
        emailToken: ''
      };

      this.registrationService.saveUser(user).subscribe({
        next: (data: User) => this.router.navigate(['/registration-success']),
        error: (error: HttpErrorResponse) => { 
          var errorString:string = error.error;
          if(error.error.includes(("email")))     this.existentEmailError = error.error; 
          if(error.error.includes("username"))    this.existentUsernameError = error.error;
        }
      });
    }
  }

  clearErrorMessage() {
    this.existentEmailError = '';
    this.existentUsernameError = '';
  }
}
